USAGE AND PIPELINE CONVERSION

Materials are made for standard 3D pipeline. 

HDRP Users will need to go to EDIT -> Render Pipeline -> "Upgrade Project Materials to High Definition Materials" to use these properly. URP Users Users will need to go to EDIT -> Render Pipeline -> "Upgrade Project Materials to High Definition Materials", Prefab Thumbnails may look odd in this configuration but prefabs and materials will work properly

URP Users will need to go to EDIT -> Render Pipeline -> "Upgrade Project Materials to UniversalRP Materials". If prefab thumbnails in the content browser turn pink after the conversion, right click and select "reimport".
