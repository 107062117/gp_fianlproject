﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cameraController : MonoBehaviour
{
    public GameObject Player;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
    void OnTriggerEnter(Collider other)
    {
        if (gameObject.name == "cameraLeft")
        {
            print("hit camera left");
            Player.GetComponent<PlayerController>().cameraHitLeft = true;
            Player.GetComponent<PlayerController>().cameraHitRight = false;
        }
        if (gameObject.name == "cameraRight")
        {
            print("hit camera right");
            Player.GetComponent<PlayerController>().cameraHitLeft = false;
            Player.GetComponent<PlayerController>().cameraHitRight = true;
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (gameObject.name == "cameraLeft")
        {
            Player.GetComponent<PlayerController>().cameraHitLeft = true;
            Player.GetComponent<PlayerController>().cameraHitRight = false;
        }
        else if (gameObject.name == "cameraRight")
        {
            Player.GetComponent<PlayerController>().cameraHitLeft = false;
            Player.GetComponent<PlayerController>().cameraHitRight = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        Player.GetComponent<PlayerController>().cameraHitLeft = false;
        Player.GetComponent<PlayerController>().cameraHitRight = false;
    }

}
