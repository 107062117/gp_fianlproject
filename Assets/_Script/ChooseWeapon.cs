﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class ChooseWeapon : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject choose_weapon;
    bool[] has_weapon = new bool[5];
    int toiletPaper = 0, acid = 1, sponge = 2, toilet = 3, toothPaste = 4;
    public RawImage[] blocks = new RawImage[5];
    public Text[] Num = new Text[5];
    public Texture2D[] icons = new Texture2D[5];
    public RawImage main_image;
    public Text description;
    public Text detailDescription;
    public Image Choose_frame;
    public Texture2D[] textures = new Texture2D[5];
    public int current_result = 0;
    public int final_result = 0;
    static public int weapon_total = 0;

    bool pick = false;

    public GameObject Player;
    private int toiletPaperNum = -2, acidNum = -2, spongeNum = -2, toiletNum = -2, toothPasteNum = -2;
    public int canUseToiletPaperTimes = 0, canUseSpongeTimes = 0, CanUseToothPasteBullet = 0, CanUseAcidTimes = 0;

    void Start()
    {
        choose_weapon.SetActive(false);
        for (int i = 0; i < 3; i++) has_weapon[i] = false;
        for (int i = 0; i < 5; i++) blocks[i].texture = null;
        for (int i = 0; i < 5; i++) Num[i].text = null;
        current_result = 0;
        final_result = 0;
        weapon_total = 0;
        Choose_frame.rectTransform.anchoredPosition3D = new Vector3(-141, -116, 0);
    }

    // Update is called once per frame
    void info(int cur_weapon)
    {
        if (cur_weapon == toiletPaperNum && toiletPaperNum != -2)
        {
            description.text = "衛生紙";
            detailDescription.text = "拿來擦屁股用的衛生紙。" + "\n" + "\n" + "不知道是誰把它沖下來了?";
        }
        else if (cur_weapon == acidNum && acidNum != -2)
        {
            description.text = "清潔劑";
            detailDescription.text = "好像是一罐腐蝕性很強的清潔用品。";
        }
        // }
        else if (cur_weapon == spongeNum && spongeNum != -2)
        {
            description.text = "馬桶通通樂";
            detailDescription.text = "可以清理塞住的馬桶。" + "\n" + "\n" + "不知道有什麼其他用途?";
            description.text = "海綿";
            detailDescription.text = "就是一塊普通的小海綿。";
        }
        else if (cur_weapon == toiletNum && toiletNum != -2)
        {
            description.text = "修好的馬桶";
            detailDescription.text = "終於修好了！" + "\n" + "\n" + "诶怎麼好像怪怪的？";
        }
        else if (cur_weapon == toothPasteNum && toothPasteNum != -2)
        {
            description.text = "牙膏";
            detailDescription.text = "薄荷口味的牙膏。" + "\n" + "\n" + "好涼好涼！";
        }
    }

    void get_weapon()
    {
        if (Player.GetComponent<PlayerController>().haveToiletPaper && !has_weapon[toiletPaperNum])
        {
            has_weapon[weapon_total] = true;
            weapon_total++;
        }
        if (Player.GetComponent<PlayerController>().haveAcid && !has_weapon[acidNum])
        {
            has_weapon[weapon_total] = true;
            weapon_total++;
        }
        if (Player.GetComponent<PlayerController>().haveSponge && !has_weapon[spongeNum])
        {
            has_weapon[weapon_total] = true;
            weapon_total++;
        }
        if (Player.GetComponent<PlayerController>().haveToilet && !has_weapon[toiletNum])
        {
            has_weapon[weapon_total] = true;
            weapon_total++;
        }
        if (Player.GetComponent<PlayerController>().haveToothPaste && !has_weapon[toothPasteNum])
        {
            has_weapon[weapon_total] = true;
            weapon_total++;
        }
    }
    // hi!

    void draw_main_image()
    {
        if (weapon_total == 0)
        {
            main_image.enabled = false;
        }
        else
        {
            if (current_result == toiletPaperNum)
                main_image.texture = textures[toiletPaper];
            else if (current_result == acidNum)
                main_image.texture = textures[acid];
            else if (current_result == spongeNum)
                main_image.texture = textures[sponge];
            else if (current_result == toiletNum)
                main_image.texture = textures[toilet];
            else if (current_result == toothPasteNum)
                main_image.texture = textures[toothPaste];
        }
    }
    void draw_icon()
    {
        for (int i = 0; i < weapon_total; i++)
        {
            if (has_weapon[i])
            {
                if (i == toiletPaperNum)
                {
                    if (canUseToiletPaperTimes > 0)
                        blocks[i].color = Color.white;
                    else
                        blocks[i].color = Color.gray;
                    blocks[i].texture = icons[toiletPaper];
                    Num[i].text = canUseToiletPaperTimes.ToString();
                }
                else if (i == acidNum)
                {
                    if (CanUseAcidTimes > 0)
                        blocks[i].color = Color.white;
                    else
                        blocks[i].color = Color.gray;
                    blocks[i].texture = icons[acid];
                    Num[i].text = CanUseAcidTimes.ToString();
                }
                else if (i == spongeNum)
                {
                    if (canUseSpongeTimes > 0)
                        blocks[i].color = Color.white;
                    else
                        blocks[i].color = Color.gray;
                    blocks[i].texture = icons[sponge];
                    Num[i].text = canUseSpongeTimes.ToString();
                }
                else if (i == toiletNum)
                {
                    blocks[i].texture = icons[toilet];
                }
                else if (i == toothPasteNum)
                {
                    if (CanUseToothPasteBullet > 0)
                        blocks[i].color = Color.white;
                    else
                        blocks[i].color = Color.gray;
                    blocks[i].texture = icons[toothPaste];
                    Num[i].text = CanUseToothPasteBullet.ToString();
                }
            }
        }
    }
    void choose_cur_weapon()
    {
        if (Input.GetKeyDown(KeyCode.U) && Input.GetMouseButton(1))
        {
            if (weapon_total > 0)
            {
                if (current_result == weapon_total - 1)
                {
                    current_result = 0;
                    Choose_frame.rectTransform.anchoredPosition3D = new Vector3(-141, -116, 0);
                }
                else
                {
                    // while (!has_weapon[current_result])
                    // {
                    current_result++;
                    //     if (current_result == weapon_total - 1)
                    //         current_result = 0;
                    // }
                    Choose_frame.rectTransform.anchoredPosition3D += new Vector3(70+current_result*1.5f, 0, 0);
                    print(Choose_frame.rectTransform.anchoredPosition3D);
                }
            }
            else
            {
                Choose_frame.rectTransform.anchoredPosition3D = new Vector3(-141, -116, 0);
            }
        }
    }
    void checkWeaponNum()
    {

    }
    void Update()
    {
        if (weapon_total == 0)
        {
            main_image.enabled = false;
            description.text = "";
        }
        else
        {
            main_image.enabled = true;
            info(current_result);
        }
        if (Input.GetMouseButton(1))
        {
            choose_weapon.SetActive(true);
        }
        else
        {
            choose_weapon.SetActive(false);
        }
        for (int i = 0; i < weapon_total; i++)
        {
            blocks[i].enabled = true;
        }
        for (int i = weapon_total; i < 5; i++)
        {
            blocks[i].enabled = false;
        }

        if (current_result == 0)
            Choose_frame.rectTransform.anchoredPosition3D = new Vector3(-141, -116, 0);
        else if (current_result == 1)
            Choose_frame.rectTransform.anchoredPosition3D = new Vector3(-69.5f, -116, 0);
        else if (current_result == 2)
            Choose_frame.rectTransform.anchoredPosition3D = new Vector3(3.5f, -116, 0);
        else if (current_result == 3)
            Choose_frame.rectTransform.anchoredPosition3D = new Vector3(78, -116, 0);
        else if (current_result == 4)
            Choose_frame.rectTransform.anchoredPosition3D = new Vector3(152.5f, -116, 0);

        toiletPaperNum = Player.GetComponent<PlayerController>().toiletPaperNum;
        acidNum = Player.GetComponent<PlayerController>().acidNum;
        spongeNum = Player.GetComponent<PlayerController>().spongeNum;
        toiletNum = Player.GetComponent<PlayerController>().toiletNum;
        toothPasteNum = Player.GetComponent<PlayerController>().toothPasteNum;

        canUseSpongeTimes = Player.GetComponent<PlayerController>().canUseSpongeTimes;
        canUseToiletPaperTimes = Player.GetComponent<PlayerController>().canUseToiletPaperTimes;
        CanUseToothPasteBullet = Player.GetComponent<PlayerController>().CanUseToothPasteBullet;
        CanUseAcidTimes = Player.GetComponent<PlayerController>().CanUseAcidTimes;

        get_weapon();
        draw_main_image();
        draw_icon();
        choose_cur_weapon();
        //Debug.Log(weapon_total);
        //final_result = index[current_result];
        final_result = current_result;
        if (weapon_total > 0)
            Player.GetComponent<PlayerController>().whichWeapon = final_result;

    }
}
