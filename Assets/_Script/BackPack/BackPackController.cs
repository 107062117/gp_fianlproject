﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BackPackController : MonoBehaviour
{
    // Start is called before the first frame update

    //index of props
    //baterry:0 firstaid:1 oldphone:2 plunger:3 
    public RawImage[] propsImg = new RawImage[12];
    public Text[] propsNumText = new Text[12];
    int[] PropsNum = new int[14];
    public Texture2D[] propsTexture = new Texture2D[14]; //all props's texture

    static public bool[] hasPropsLocal = new bool[14];//whether has that prop or not
    static public int totalProps = 0; //how many props
    static public int[] blockContent = new int[12];
    public PlayerController player;

    void Start()
    {
        for (int i = 0; i < 12; i++)
        {
            hasPropsLocal[i] = false;
            propsImg[i].enabled = false;
            propsNumText[i].enabled = false;
            blockContent[i] = 0;
        }
        totalProps = 0;

    }
    void DrawIcon()
    {
        for (int i = 0; i < totalProps; i++)
        {
            if (hasPropsLocal[blockContent[i]])
            {
                propsImg[i].enabled = true;
                propsNumText[i].enabled = true;
                propsImg[i].texture = propsTexture[blockContent[i]];
                if (PropsNum[blockContent[i]] == 0)
                {
                    // Debug.Log("black");
                    propsImg[i].color = Color.gray;
                }
                else
                {
                    propsImg[i].color = Color.white;
                }
                //write number
                propsNumText[i].text = PropsNum[blockContent[i]].ToString();
            }
            else
            {
                propsImg[i].enabled = false;
                propsNumText[i].enabled = false;
            }

        }
    }
    static public void HasWhatProp()
    {
        totalProps = 0;
        for (int i = 0; i < 14; i++)
        {
            if (hasPropsLocal[i])
            {
                blockContent[totalProps] = i;
                totalProps += 1;
            }
        }
    }
    void CheckNumber()
    {

        int[] tmp = new int[14];
        for (int i = 0; i < 14; i++)
        {
            if (PropsNum[i] == 0 && hasPropsLocal[i])
            {
                //propsImg[i].color = new Color(1,1,1);
                break;
            }
        }
    }


    // Update is called once per frame
    void Update()
    {
        CheckNumber();
        DrawIcon();
        //Debug.Log(blockContent[0]);
        //rePosition();
        //Debug.Log("total " + totalProps);
    }
    static public int getIndex(string item)
    {
        switch (item)
        {
            case "battery":
                return 0;
            case "firstaid":
                return 1;
            case "Old Phone":
                return 2;
            case "plunger":
                return 3;
            case "bone":
                return 4;
            case "comb":
                return 5;
            case "skinCream":
                return 6;
            case "mushroom":
                return 7;
            case "translator":
                return 8;
            case "SimpleSkeleton":
                return 9;
            case "sock":
                return 10;
            case "candy":
                return 11;
            case "Gingerbread":
                return 12;
            case "nerdbook":
                return 13;
            default:
                print("Prop not found");
                return -1;
        }
    }


    public void getProp(string str)
    {
        int index = getIndex(str);
        PropsNum[index] += 1;
        hasPropsLocal[index] = true;
        HasWhatProp();
        print(str + " get:" + PropsNum[index]);
    }
    public void deleteProp(int i)
    {
        if (totalProps > i && PropsNum[blockContent[i]] > 0)
        {
            PropsNum[blockContent[i]] -= 1;
            print("delete:" + blockContent[i] + PropsNum[blockContent[i]]);
            switch (blockContent[i])
            {
                case 0:
                    player.OpenConnection();
                    break;
                case 1:
                    player.addLife();
                    break;
                case 3:
                    player.plunge();
                    break;
                case 7:
                    player.useMushroom();
                    break;
                case 9:
                    player.callSkeleton();
                    break;
                default:
                    print("notiong to do with");
                    break;
            }
        }

    }
    public void deletePropByDoor(string[] items)
    {
        for (int i = 0; i < items.Length; i++)
        {
            int index = getIndex(items[i]);
            PropsNum[index] -= 1;
            PropsNum[index] = Mathf.Max(0, PropsNum[index]);
        }
    }
    public void deletePropString(string str)
    {
        int index = getIndex(str);
        PropsNum[index] -= 1;
        PropsNum[index] = Mathf.Max(0, PropsNum[index]);
    }

    // return props number of items with Aray
    public int[] checkProps(string[] items)
    {
        int[] ans = new int[items.Length];
        for (int i = 0; i < items.Length; i++)
        {
            int index = getIndex(items[i]);
            ans[i] = PropsNum[index];
        }
        return ans;
    }

}
