﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class BlockButtons : MonoBehaviour
{
    // Start is called before the first frame update
    public BackPackController BackPackController;
    public GameObject backpackPanel;

    private GameController gameController;

    // Update is called once per frame
    void Start()
    {
        gameController = GameObject.Find("GameController").GetComponent<GameController>();
    }
    void Update()
    {

    }
    public void Blocks1Pressed()
    {
        BackPackController.deleteProp(0);
        closePanel();
    }
    public void Blocks2Pressed()
    {
        BackPackController.deleteProp(1);
        closePanel();
    }
    public void Blocks3Pressed()
    {
        BackPackController.deleteProp(2);
        closePanel();
    }
    public void Blocks4Pressed()
    {
        BackPackController.deleteProp(3);
        closePanel();
    }
    public void Blocks5Pressed()
    {
        BackPackController.deleteProp(4);
        closePanel();
    }
    public void Blocks6Pressed()
    {
        BackPackController.deleteProp(5);
        closePanel();
    }
    public void Blocks7Pressed()
    {
        BackPackController.deleteProp(6);
        closePanel();
    }
    public void Blocks8Pressed()
    {
        BackPackController.deleteProp(7);
        closePanel();
    }
    public void Blocks9Pressed()
    {
        BackPackController.deleteProp(8);
        closePanel();
    }
    public void Blocks10Pressed()
    {
        BackPackController.deleteProp(9);
        closePanel();
    }
    public void Blocks11Pressed()
    {
        BackPackController.deleteProp(10);
        closePanel();
    }
    public void Blocks12Pressed()
    {
        BackPackController.deleteProp(11);
        closePanel();
    }
    private void closePanel()
    {
        GameController.canAttack = true;
        GameController.openBackPack = false;
        print(GameController.canAttack);
        backpackPanel.SetActive(false);
    }

}
