﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class GameController : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject BackPack;
    static public bool canAttack = true;
    static public bool openBackPack = false;
    private GameObject player;
    public GameObject spider_Queen, santa, skeleton;
    public GameObject w1, w2, w3;
    private void Awake()
    {
        player = GameObject.Find("Player");
        if (santa)
            santa.SetActive(false);
        if (skeleton)
            skeleton.SetActive(false);
        if (spider_Queen)
            spider_Queen.SetActive(false);
    }
    void Start()
    {
        BackPack.SetActive(false);
        canAttack = true;
        openBackPack = false;
    }

    // Update is called once per frame
    public void backpackButton()
    {
        if (BackPack.activeSelf == false)
        {
            BackPack.SetActive(true);
            canAttack = false;
            openBackPack = true;
        }
        else
        {
            BackPack.SetActive(false);
            openBackPack = false;
            canAttack = true;
        }
    }

    public void mouseInBackPackButton()
    {
        canAttack = false;
    }
    public void mouseOutBackPackButton()
    {
        canAttack = true;
    }
    public void backToMenu()
    {
        SceneManager.LoadScene(0);
    }
    void Update()
    {
        if (openBackPack) canAttack = false;
        if (player.transform.position.x < 80) w1.SetActive(true);
        else w1.SetActive(false);
        if (player.transform.position.x > 176 && player.transform.position.x < 287) w2.SetActive(true);
        else w2.SetActive(false);
        if (player.transform.position.x > 340) w3.SetActive(true);
        else w3.SetActive(false);
        if (winPhoneController.winConplete)
        {
            //go to win scene
            SceneManager.LoadScene(2);
        }
        if (player.transform.position.x < 150)
        {
            if (spider_Queen)
                spider_Queen.SetActive(true);
        }
        else
        {
            if (spider_Queen)
                spider_Queen.SetActive(false);
        }
        if (player.transform.position.x >= 189 && player.transform.position.x < 210)
        {
            if (skeleton)
                skeleton.SetActive(true);
        }
        else
        {
            if (skeleton)
                skeleton.SetActive(false);
        }

        if (player.transform.position.x >= 300)
        {
            if (santa)
                santa.SetActive(true);
        }
        else
        {
            if (santa)
                santa.SetActive(false);
        }
        if (Input.GetKeyDown(KeyCode.Alpha1))
            player.transform.position = new Vector3(6, 1, -32);//init
        else if (Input.GetKeyDown(KeyCode.Alpha2))
            player.transform.position = new Vector3(63, 0.8f, -52);//spider door
        else if (Input.GetKeyDown(KeyCode.Alpha3))
            player.transform.position = new Vector3(128, 0.8f, -60);//spider room
        else if (Input.GetKeyDown(KeyCode.Alpha4))
            player.transform.position = new Vector3(44, 0.8f, -63);//old phone
        else if (Input.GetKeyDown(KeyCode.Alpha5))
            player.transform.position = new Vector3(188, 1f, -72);//maze2
        else if (Input.GetKeyDown(KeyCode.Alpha6))
            player.transform.position = new Vector3(317, 1.47f, -51);//santa room
        else if (Input.GetKeyDown(KeyCode.Alpha7))
            player.transform.position = new Vector3(358, 1f, -66);//maze3
        else if (Input.GetKeyDown(KeyCode.Alpha8))
            player.transform.position = new Vector3(398, 0.8f, -74);//swamp
        else if (Input.GetKeyDown(KeyCode.Alpha9))
            player.transform.position = new Vector3(418, 0.8f, -70);//end


    }
}