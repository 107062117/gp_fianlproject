﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class doorPanel : MonoBehaviour
{
    public GameObject PropPanel;
    public GameObject leaveCheckPanel;
    public GameObject[] Panels;
    public Sprite[] sprites = new Sprite[13];
    private GameObject currentDoor;
    void Start()
    {
        gameObject.SetActive(true);
        leaveCheckPanel.SetActive(false);
        PropPanel.SetActive(false);
    }
    public void openPropPanel(string[] items, int[] num)
    {
        PropPanel.SetActive(true);
        for (int i = 0; i < items.Length; i++)
        {
            Image img = Panels[i].transform.Find("img").gameObject.GetComponent<Image>();
            Text text = Panels[i].transform.Find("Text").gameObject.GetComponent<Text>();
            Panels[i].transform.Find("num").gameObject.GetComponent<Text>().text = num[i].ToString();
            switch (items[i])
            {
                case "battery":
                    img.sprite = sprites[0];
                    text.text = "電池";
                    break;
                case "firstaid":
                    img.sprite = sprites[1];
                    text.text = "急救箱";
                    break;
                case "Old Phone":
                    img.sprite = sprites[2];
                    text.text = "舊手機";
                    break;
                case "plunger":
                    img.sprite = sprites[3];
                    text.text = "通通樂";
                    break;
                case "bone":
                    img.sprite = sprites[4];
                    text.text = "骨頭";
                    break;
                case "comb":
                    img.sprite = sprites[5];
                    text.text = "梳子";
                    break;
                case "skinCream":
                    img.sprite = sprites[6];
                    text.text = "乳液";
                    break;
                case "mushroom":
                    img.sprite = sprites[7];
                    text.text = "歡樂蘑菇";
                    break;
                case "translator":
                    img.sprite = sprites[8];
                    text.text = "翻譯機";
                    break;
                case "skeletonCall":
                    img.sprite = sprites[9];
                    text.text = "骷髏";
                    break;
                case "sock":
                    img.sprite = sprites[10];
                    text.text = "聖誕襪";
                    break;
                case "candy":
                    img.sprite = sprites[11];
                    text.text = "拐杖糖";
                    break;
                case "Gingerbread":
                    img.sprite = sprites[12];
                    text.text = "小薑餅人";
                    break;
                default:
                    break;
            }
        }
    }
    public void closePropPanel()
    {
        if (PropPanel.activeSelf)
            PropPanel.SetActive(false);
    }

    public void showLeavePanel(GameObject door)
    {
        leaveCheckPanel.SetActive(true);
        GameController.canAttack = false;
        currentDoor = door;
    }
    public void sureToLeave()
    {
        currentDoor.GetComponent<doorcontroller>().leave();
        leaveCheckPanel.SetActive(false);
        closePropPanel();
    }
    public void closePanel()
    {
        leaveCheckPanel.SetActive(false);
        GameController.canAttack = true;
    }
    public void NotToLeave()
    {
        if (leaveCheckPanel.activeSelf)
            leaveCheckPanel.SetActive(false);
        closePropPanel();
    }

    // Update is called once per frame
    void Update()
    {
        if (leaveCheckPanel.activeSelf)
        {
            if (Input.GetKeyDown(KeyCode.Y))
            {
                sureToLeave();
            }
            else if (Input.GetKeyDown(KeyCode.N))
                NotToLeave();
        }
    }
}
