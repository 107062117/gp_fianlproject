﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class menuController : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject instructionPanel;
    public GameObject[] stuffs;
    public GameObject butStart;
    public GameObject butInstr;
    public GameObject butExit;
    public GameObject loadingImage;
    public Text LoadingText;
    int time = 0;
    bool gotoPlay = false;
    AsyncOperation loadingOperation;
    public Slider slider;
    public AudioSource audioSource;
    public AudioClip flush;
    void Start()
    {
        instructionPanel.SetActive(false);
        stuffs[0].SetActive(true);
        stuffs[1].SetActive(true);
        stuffs[2].SetActive(true);
        loadingImage.SetActive(false);
        time = 0;
        gotoPlay = false;
    }
    void Update()
    {
        if(gotoPlay){
            slider.value = Mathf.Clamp01(loadingOperation.progress / 0.9f);
            float progressValue = Mathf.Clamp01(loadingOperation.progress / 0.9f);
            Debug.Log(progressValue);
        }
    }
    // Update is called once per frame
    public void mouseInStartButton(){
        butStart.gameObject.transform.localScale = new Vector3(1.2f,1.2f,1f);
    }
    public void mouseOutStartButton(){
        butStart.gameObject.transform.localScale = new Vector3(1,1,1f);
    }
    public void mouseInInstrButton(){
        butInstr.gameObject.transform.localScale = new Vector3(1.2f,1.2f,1f);
    }
    public void mouseOutInstrButton(){
        butInstr.gameObject.transform.localScale = new Vector3(1,1,1f);
    }
    public void mouseInExitButton(){
        butExit.gameObject.transform.localScale = new Vector3(1.2f,1.2f,1f);
    }
    public void mouseOutExitButton(){
        butExit.gameObject.transform.localScale = new Vector3(1,1,1f);
    }
    public void startGame(){
        //go to play
        gotoPlay = true;
        audioSource.PlayOneShot(flush);
        loadingImage.SetActive(true);
        loadingOperation = SceneManager.LoadSceneAsync(1);
    }
    public void openInstruction(){
        instructionPanel.SetActive(true);
        butInstr.gameObject.transform.localScale = new Vector3(1,1,1f);
        stuffs[0].SetActive(false);
        stuffs[1].SetActive(false);
        stuffs[2].SetActive(false);
    }
    public void closeInstruction(){
        instructionPanel.SetActive(false);
        stuffs[0].SetActive(true);
        stuffs[1].SetActive(true);
        stuffs[2].SetActive(true);
    }

}
