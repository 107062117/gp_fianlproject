﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class pickUpPanel : MonoBehaviour
{
    public Text tname;
    public Text type;
    public Image image;
    public Sprite[] images;
    private bool isShow;
    int showTime = 100;
    public GameObject panel;
    // Start is called before the first frame update
    void Start()
    {
        isShow = false;
        panel.SetActive(false);
    }
    void Update()
    {
        if (isShow)
        {
            showTime--;
            if (showTime <= 0)
            {
                isShow = false;
                panel.SetActive(false);
            }
        }
    }
    
    public void show(string n, string t)
    {
        panel.SetActive(true);
        showTime = 100;
        isShow = true;
        switch (t)
        {
            case "Weapon":
                type.text = "武器";
                break;
            case "Prop":
                type.text = "道具";
                break;
        }
        switch (n)
        {
            case "plunger":
                image.sprite = images[0];
                tname.text = "通通樂";
                break;
            case "toilet paper":
                image.sprite = images[1];
                tname.text = "衛生紙";
                break;
            case "acid":
                image.sprite = images[2];
                tname.text = "清潔劑";
                break;
            case "battery":
                image.sprite = images[3];
                tname.text = "電池";
                break;
            case "firstaid":
                image.sprite = images[4];
                tname.text = "急救箱";
                break;
            case "Old Phone":
                image.sprite = images[5];
                tname.text = "舊手機";
                break;
            case "bone":
                image.sprite = images[6];
                tname.text = "骨頭";
                break;
            case "sponge":
                image.sprite = images[7];
                tname.text = "海綿";
                break;
            case "comb":
                image.sprite = images[8];
                tname.text = "梳子";
                break;
            case "skinCream":
                image.sprite = images[9];
                tname.text = "乳液";
                break;
            case "mushroom":
                image.sprite = images[10];
                tname.text = "歡樂蘑菇";
                break;
            case "translator":
                image.sprite = images[11];
                tname.text = "翻譯機";
                break;
            case "SimpleSkeleton":
                image.sprite = images[12];
                tname.text = "骷髏";
                break;
            case "sock":
                image.sprite = images[13];
                tname.text = "聖誕襪";
                break;
            case "candy":
                image.sprite = images[14];
                tname.text = "拐杖糖";
                break;
            case "toothPaste":
                image.sprite = images[15];
                tname.text = "牙膏";
                break;
            case "BrokenToilet":
                image.sprite = images[16];
                tname.text = "修好的馬桶";
                break;
            case "Gingerbread":
                image.sprite = images[17];
                tname.text = "小薑餅人";
                break;
            case "nerdbook":
                image.sprite = images[18];
                tname.text = "肥宅雜誌";
                break;
                // default:
                //     print("image not found");
                //     isShow = false;
                //     break;
        }
    }

}
