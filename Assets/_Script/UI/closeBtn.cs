﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class closeBtn : MonoBehaviour
{
    public GameObject Paper;
    public GameObject OldBook;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    public void closePaper()
    {
        Paper.SetActive(false);
    }

    public void closeOldBook()
    {
        OldBook.SetActive(false);
    }
}
