﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class splash_collision : MonoBehaviour
{
    private ParticleSystem part;
    public List<ParticleCollisionEvent> collisionEvents;
    void Start()
    {
        part = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
    }
    private void OnParticleCollision(GameObject other)
    {
        int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents);
        Rigidbody rb = other.GetComponent<Rigidbody>();
        int i = 0;

        while (i < numCollisionEvents)
        {
            if (rb)
            {
                if (other.tag == "enemy")
                {
                    switch (other.name)
                    {
                        case "Bat1":
                            other.GetComponent<bat>().getHit(5);
                            break;
                        case "SantaClaus":
                            other.GetComponent<Santa>().getHit(5);
                            break;
                        case "SimpleSkeleton":
                            other.GetComponent<skeleton>().getHit(5);
                            break;
                        case "spider":
                            other.GetComponent<spider>().getHit(5);
                            break;
                        case "Spider_Queen":
                            other.GetComponent<SpiderQueen>().getHit(5);
                            break;
                        default:
                            break;
                    }
                }
                break;
            }
            i++;
        }
    }
}
