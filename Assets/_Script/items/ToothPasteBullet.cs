﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ToothPasteBullet : MonoBehaviour
{
    private int hit = 3;
    private float speed = 0.6f;
    void Start()
    {
        Destroy(gameObject, 2);
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player") return;
        if (other.gameObject.name == "Bat1")
            other.gameObject.GetComponent<bat>().getHit(hit);
        else if (other.gameObject.name == "SantaClaus")
            other.gameObject.GetComponent<Santa>().getHit(hit);
        else if (other.gameObject.name == "SimpleSkeleton")
            other.gameObject.GetComponent<skeleton>().getHit(hit);
        else if (other.gameObject.name == "spider")
            other.gameObject.GetComponent<spider>().getHit(hit);
        else if (other.gameObject.name == "Spider_Queen")
            other.gameObject.GetComponent<SpiderQueen>().getHit(hit);
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.Translate(0, 0, speed);
    }
}
