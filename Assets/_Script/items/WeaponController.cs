﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using Random = UnityEngine.Random;

public class WeaponController : MonoBehaviour
{
    public GameObject Player;
    public GameObject GameController;
    public GameObject pickUpText;
    private GameObject pickUpEffect;
    public GameObject pickUpPanel;

    /**************************************/
    public AudioSource audioSource;
    public AudioClip pickUpSE;
    /**************************************/

    private bool canPick = false;
    private string[] weaponList = { "plunger", "toilet paper", "toilet", "sponge", "toothPaste" };
    private string weapon;
    private int num = -2;
    static int weaponNum = -1;


    void Start()
    {
        pickUpEffect = transform.Find("pickUpEffect").gameObject;
        pickUpEffect.SetActive(false);
        pickUpText.SetActive(false);
        weapon = "";
        weaponNum = -1;
        num = -2;
    }

    // Update is called once per frame
    void Update()
    {
        double dist = Math.Sqrt(Math.Pow((transform.position.x - Player.transform.position.x), 2) + Math.Pow((transform.position.z - Player.transform.position.z), 2));
        if (dist <= 5)
        {
            pickUpEffect.SetActive(true);
        }
        else
        {
            pickUpEffect.SetActive(false);
        }

        if (canPick)
        {
            if (Input.GetKeyDown(KeyCode.P))
            {
                audioSource.PlayOneShot(pickUpSE);
                switch (weapon)
                {
                    case "toilet paper":
                        Player.GetComponent<PlayerController>().haveToiletPaper = true;
                        if (Player.GetComponent<PlayerController>().toiletPaperNum == -2)
                        {
                            weaponNum++;
                            Player.GetComponent<PlayerController>().whichWeapon = weaponNum;
                            Player.GetComponent<PlayerController>().toiletPaperNum = weaponNum;
                            num = weaponNum;
                        }
                        else
                        {
                            num = Player.GetComponent<PlayerController>().toiletPaperNum;
                            Player.GetComponent<PlayerController>().whichWeapon = num;
                        }
                        Player.GetComponent<PlayerController>().canUseToiletPaperTimes += 3;
                        break;
                    case "sponge":
                        Player.GetComponent<PlayerController>().haveSponge = true;
                        if (Player.GetComponent<PlayerController>().spongeNum == -2)
                        {
                            weaponNum++;
                            Player.GetComponent<PlayerController>().whichWeapon = weaponNum;
                            Player.GetComponent<PlayerController>().spongeNum = weaponNum;
                            num = weaponNum;
                        }
                        else
                        {
                            num = Player.GetComponent<PlayerController>().spongeNum;
                            Player.GetComponent<PlayerController>().whichWeapon = num;
                        }
                        Player.GetComponent<PlayerController>().canUseSpongeTimes += 5;
                        break;
                    case "BrokenToilet":
                        bool isfixed = transform.Find("trigger").gameObject.GetComponent<toiletBroken>().isFixed;
                        if (isfixed)
                        {
                            Player.GetComponent<PlayerController>().haveToilet = true;
                            if (Player.GetComponent<PlayerController>().toiletNum == -2)
                            {
                                weaponNum++;
                                Player.GetComponent<PlayerController>().whichWeapon = weaponNum;
                                Player.GetComponent<PlayerController>().toiletNum = weaponNum;
                                num = weaponNum;
                            }
                            else
                            {
                                num = Player.GetComponent<PlayerController>().toiletNum;
                                Player.GetComponent<PlayerController>().whichWeapon = num;
                            }
                        }
                        else
                        {
                            print("not fixed");
                            return;
                        }

                        break;
                    case "toothPaste":
                        Player.GetComponent<PlayerController>().haveToothPaste = true;
                        if (Player.GetComponent<PlayerController>().toothPasteNum == -2)
                        {
                            weaponNum++;
                            Player.GetComponent<PlayerController>().whichWeapon = weaponNum;
                            Player.GetComponent<PlayerController>().toothPasteNum = weaponNum;
                            num = weaponNum;
                        }
                        else
                        {
                            num = Player.GetComponent<PlayerController>().toothPasteNum;
                            Player.GetComponent<PlayerController>().whichWeapon = num;
                        }
                        Player.GetComponent<PlayerController>().CanUseToothPasteBullet += 10;
                        break;
                    case "acid":
                        Player.GetComponent<PlayerController>().haveAcid = true;
                        if (Player.GetComponent<PlayerController>().acidNum == -2)
                        {
                            weaponNum++;
                            Player.GetComponent<PlayerController>().whichWeapon = weaponNum;
                            Player.GetComponent<PlayerController>().acidNum = weaponNum;
                            num = weaponNum;
                        }
                        else
                        {
                            num = Player.GetComponent<PlayerController>().acidNum;
                            Player.GetComponent<PlayerController>().whichWeapon = num;
                        }
                        Player.GetComponent<PlayerController>().CanUseAcidTimes += 7;
                        break;
                    default:
                        print("in Weapon Controller: weapon not found");
                        break;
                }
                print("get: " + weapon);
                GameController.GetComponent<ChooseWeapon>().current_result = num;
                GameController.GetComponent<ChooseWeapon>().final_result = num;
                //pickUpPanel.SetActive(true);
                pickUpPanel.GetComponent<pickUpPanel>().show(weapon, "Weapon");
                pickUpText.SetActive(false);
                canPick = false;
                Destroy(gameObject, 0.05f);
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            weapon = gameObject.name;
            canPick = true;
            if (gameObject.name != "BrokenToilet")
                pickUpText.GetComponent<Text>().text = "按 'P' 拾起";
            pickUpText.SetActive(true);
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            canPick = true;
            weapon = gameObject.name;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            weapon = "";
            canPick = false;
            pickUpText.SetActive(false);
        }
    }
}
