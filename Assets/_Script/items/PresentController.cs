﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PresentController : MonoBehaviour
{
    public bool notKinematic;
    public int type;
    public GameObject item;
    // type0: run
    // type1: tool (not for open the door!)
    // type2: key
    // type3: santa's book
    // assign after instantiate


    private GameObject santa;
    private GameObject[] goals = new GameObject[3];
    private GameObject goal;
    private Rigidbody rigid;
    private AnimatorStateInfo stateinfo;
    private bool isthrow;

    public GameObject nerdbook;

    /******************************/
    public AudioClip SantaHoHoHoSE;
    public AudioClip getPresentSE;
    private AudioSource audioSource;

    void Start()
    {
        santa = GameObject.Find("SantaClaus");
        goals[0] = GameObject.Find("SantaClaus/goal0");
        goals[1] = GameObject.Find("SantaClaus/goal1");
        goals[2] = GameObject.Find("SantaClaus/goal2");
        if (gameObject.name != "present_nerdbook")
            rigid = gameObject.GetComponent<Rigidbody>();
        isthrow = false;
        // notKinematic = false;
        if (gameObject.tag == "present") Destroy(gameObject, 15);
        if (gameObject.name != "present_nerdbook")
            nerdbook = GameObject.Find("xmas room 1/items/nerdbook");
        //nerdbook.SetActive(false);
        audioSource = GameObject.Find("Audio Source").GetComponent<AudioSource>();
    }

    void Update()
    {
        // Throw present
        if (gameObject.name != "present_nerdbook")
        {
            if (notKinematic)
            {
                rigid.isKinematic = false;
                if (!isthrow)
                {
                    int idx = (int)Mathf.Ceil(Random.Range(0.0001f, goals.Length)) - 1;
                    goal = goals[idx];
                    Vector3 dir = goal.transform.position - gameObject.transform.position;
                    rigid.velocity = dir * 3f;
                    isthrow = true;
                }
            }
            else rigid.isKinematic = true;
        }
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name == "Player" && gameObject.tag == "present")
        {
            if (type == 0)
            {
                AnimatorStateInfo stateinfo = santa.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0);
                // Player touchs the present
                if (other.gameObject.name == "Player" && santa.GetComponent<Santa>().CanAction && !stateinfo.IsName("run"))
                {
                    print("run after");
                    audioSource.PlayOneShot(SantaHoHoHoSE);
                    santa.GetComponent<Animator>().SetBool("run", true);
                    santa.GetComponent<Santa>().fire_effect.SetActive(true);
                    santa.GetComponent<Santa>().close();
                    santa.transform.LookAt(other.gameObject.transform.position);
                }
            }
            else
            {
                audioSource.PlayOneShot(getPresentSE);
                float x = gameObject.transform.position.x;
                float y = gameObject.transform.position.y + 0.25f;
                float z = gameObject.transform.position.z;
                item.SetActive(true);
                item.transform.position = new Vector3(x, y, z);
                if (item.name == "Gingerbread" || item.name == "nerdbook")
                {
                    x = other.gameObject.transform.position.x;
                    y = other.gameObject.transform.position.y;
                    z = other.gameObject.transform.position.z;
                    item.transform.LookAt(new Vector3(x, y, z));
                }

            }
            Destroy(gameObject);
        }
        else if (other.gameObject.name == "Player" && gameObject.tag == "present_nerdbook")
        {
            audioSource.PlayOneShot(getPresentSE);
            nerdbook.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}
