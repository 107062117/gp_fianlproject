﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using Random = UnityEngine.Random;

public class doorcontroller : MonoBehaviour
{
    /*******/
    private GameObject Player;
    /*******/
    public doorPanel doorPanel;
    public GameObject bookcase;
    public BackPackController PropBackPack;
    private Animator animator;
    public Vector3 transition_position;
    public string[] openCondition; // If you don't want condition, just set it to 0 in Scene.
    public bool isopen = false;

    /******************************/
    public AudioSource audioSource;
    public AudioClip bookcaseOpenSE;


    void Start()
    {
        if (gameObject.name == "exit_Xmas")
            animator = bookcase.GetComponent<Animator>();
        Player = GameObject.Find("Player");
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            if (isopen)
            {
                if (gameObject.name == "exit_Xmas")
                {
                    audioSource.PlayOneShot(bookcaseOpenSE);
                    animator.SetBool("isopen", true);
                    doorPanel.showLeavePanel(gameObject);
                }
                else
                {
                    leaveCheck();
                }
                return;
            }

            int[] num = PropBackPack.checkProps(openCondition);
            // print(num.Length);
            for (int i = 0; i < num.Length; i++)
            {
                if (num[i] == 0)//not satisfied yet
                {
                    doorPanel.openPropPanel(openCondition, num);
                    return;
                }
            }
            leaveCheck();//can leave
        }
    }

    private void OnCollisionExit(Collision other)
    {
        doorPanel.closePropPanel();
        doorPanel.closePanel();


    }

    private void leaveCheck()
    {
        doorPanel.showLeavePanel(gameObject);
    }
    public void leave()//in doorPanel, if SureToLeave(), then call leave()
    {
        if (!isopen) isopen = true;
        if (gameObject.name == "exit_Xmas")
        {
            animator.SetBool("isopen", false);
            Player.GetComponent<PlayerController>().holdingCandle = false;
            Player.transform.position = transition_position;
            GameController.canAttack = true;
            print("leave");
        }
        else
        {
            Player.transform.position = transition_position;
            PropBackPack.deletePropByDoor(openCondition);
            GameController.canAttack = true;
            print("leave");
        }

    }
}
