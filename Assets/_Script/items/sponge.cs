﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class sponge : MonoBehaviour
{
    int hit = 1;

    void Start()
    {
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player") return;
        if (other.gameObject.name == "Bat1")
            other.gameObject.GetComponent<bat>().getHit(hit);
        else if (other.gameObject.name == "SantaClaus")
            other.gameObject.GetComponent<Santa>().getHit(hit);
        else if (other.gameObject.name == "SimpleSkeleton")
            other.gameObject.GetComponent<skeleton>().getHit(hit);
        else if (other.gameObject.name == "spider")
            other.gameObject.GetComponent<spider>().getHit(hit);
        else if (other.gameObject.name == "Spider_Queen")
            other.gameObject.GetComponent<SpiderQueen>().getHit(hit);
        Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
