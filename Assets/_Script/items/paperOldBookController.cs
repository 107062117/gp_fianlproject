﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using Random = UnityEngine.Random;

public class paperOldBookController : MonoBehaviour
{
    public GameObject text;
    public GameObject Paper;
    public GameObject OldBook;
    // public GameObject wardrobe;
    // private Animator animator;

    private bool canReadPaper = false;
    private bool canReadBook = false;
    // private bool open = false;


    // Start is called before the first frame update
    void Start()
    {
        // animator = wardrobe.GetComponent<Animator>();
        text.SetActive(false);
        Paper.SetActive(false);
        OldBook.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (canReadPaper && Input.GetKey(KeyCode.P))
        {
            // open = true;
            canReadBook = false;
            Paper.SetActive(true);
            text.SetActive(false);
            canReadPaper = false;
        }

        if (canReadBook && Input.GetKey(KeyCode.P))
        {
            canReadPaper = false;
            OldBook.SetActive(true);
            text.SetActive(false);
            canReadBook = false;
        }

        //animator.SetBool("open", open);
    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.name == "Player")
        {
            text.SetActive(true);
            text.GetComponent<Text>().text = "?";
            if (gameObject.name == "paper")
                canReadPaper = true;
            else if (gameObject.name == "book")
                canReadBook = true;
        }
    }

    void OnCollisionExit(Collision other)
    {
        text.SetActive(false);
        canReadPaper = false;
        canReadBook = false;
    }

}
