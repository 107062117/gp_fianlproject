﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using Random = UnityEngine.Random;


public class treasureController : MonoBehaviour
{
    /***********/
    private GameObject Player;
    public int type = 0;
    /***********/
    public GameObject pickUpText;
    private GameObject pickUpEffect;
    public GameObject pickUpPanel;
    public BackPackController PropBackPack;
    /**************************************/
    public AudioSource audioSource;
    public AudioClip pickUpSE;
    /**************************************/

    private bool canPick = false;
    private string treasure;
    // private string[] treasureList = { "plunger", "battery", "firstaid", "Old Phone", "bone", "sock", "candy", "Gingerbread", "nerdbook" };

    void Awake()
    {
        Player = GameObject.Find("Player");
    }

    void Start()
    {
        if (gameObject.name != "candle")
        {
            pickUpEffect = transform.Find("pickUpEffect").gameObject;
            pickUpEffect.SetActive(false);
        }
        pickUpText.SetActive(false);
        treasure = "";
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.name != "candle")
        {
            double dist = Math.Sqrt(Math.Pow((transform.position.x - Player.transform.position.x), 2) + Math.Pow((transform.position.z - Player.transform.position.z), 2));
            if (dist <= 5)
            {
                pickUpEffect.SetActive(true);
            }
            else
            {
                pickUpEffect.SetActive(false);
            }
        }

        if (canPick)
        {
            if (Input.GetKey(KeyCode.P))
            {
                audioSource.PlayOneShot(pickUpSE);
                if (treasure == "nerdbook")
                    Player.GetComponent<PlayerController>().getNerdBook = true;
                else if (treasure == "translator")
                    Player.GetComponent<PlayerController>().getTranslator = true;
                else if (treasure == "Old Phone")
                    Player.GetComponent<PlayerController>().getOldPhone = true;


                if (treasure == "candle")
                    Player.GetComponent<PlayerController>().holdingCandle = true;
                else
                {
                    PropBackPack.getProp(treasure);
                    //pickUpPanel.SetActive(true);
                    pickUpPanel.GetComponent<pickUpPanel>().show(treasure, "Prop");
                }

                pickUpText.SetActive(false);
                canPick = false;
                if (type == 2 || type == 3) Destroy(gameObject);
                else gameObject.SetActive(false);
            }
        }

    }
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            treasure = gameObject.name;
            canPick = true;
            pickUpText.GetComponent<Text>().text = "按 'P' 拾起";
            pickUpText.SetActive(true);
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            canPick = true;
            pickUpText.SetActive(true);
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            treasure = "";
            canPick = false;
            pickUpText.SetActive(false);
        }
    }
}
