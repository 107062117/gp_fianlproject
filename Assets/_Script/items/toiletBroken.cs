﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using Random = UnityEngine.Random;

public class toiletBroken : MonoBehaviour
{
    private bool isFixing;
    public bool isFixed = false;
    private GameObject poo;
    public GameObject pickUpText;
    private GameObject player;
    void Start()
    {
        isFixing = false;
        poo = transform.parent.Find("poo").gameObject;
        poo.SetActive(false);
        player = gameObject.transform.parent.GetComponent<WeaponController>().Player;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            pickUpText.GetComponent<Text>().text = "這是個壞掉的馬桶，請修好它";
            pickUpText.SetActive(true);
            isFixing = true;
            poo.SetActive(false);
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            pickUpText.SetActive(false);
            isFixing = false;
            poo.SetActive(false);
        }
    }

    void Update()
    {
        if (isFixing)
        {
            if (player.GetComponent<PlayerController>().isPlunge)
            {
                isFixed = true;
            }
            if (isFixed == true && !player.GetComponent<PlayerController>().isPlunge)
            {
                poo.SetActive(true);
                pickUpText.GetComponent<Text>().text = "按 'P' 拾起";
            }
        }
    }
}

