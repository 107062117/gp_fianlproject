﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
public class winSceneController : MonoBehaviour
{
    // Start is called before the first frame update
    public GameObject butreStart;
    public GameObject butBack;
    AsyncOperation loadingOperation;

    bool gotoPlay = false;

    public Slider slider;
    public GameObject loading;
    void Start()
    {
        gotoPlay = false;
        loading.SetActive(false);
    }
    public void mouseInreStartButton(){
        butreStart.gameObject.transform.localScale = new Vector3(1.2f,1.2f,1f);
    }
    public void mouseOutreStartButton(){
        butreStart.gameObject.transform.localScale = new Vector3(1,1,1f);
    }
    public void mouseInBackButton(){
        butBack.gameObject.transform.localScale = new Vector3(1.2f,1.2f,1f);
    }
    public void mouseOutBackButton(){
        butBack.gameObject.transform.localScale = new Vector3(1,1,1f);
    }
    public void pressReStart(){
        gotoPlay = true;
        loading.SetActive(true);
        loadingOperation = SceneManager.LoadSceneAsync(1);
    }
    public void PressBack(){
        SceneManager.LoadScene(0);
    }
    // Update is called once per frame
    void Update()
    {
        if(gotoPlay){
            slider.value = Mathf.Clamp01(loadingOperation.progress / 0.9f);
            float progressValue = Mathf.Clamp01(loadingOperation.progress / 0.9f);
            Debug.Log(progressValue);
        }
    }
}
