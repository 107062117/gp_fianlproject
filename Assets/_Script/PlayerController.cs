﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
// using Random = UnityEngine.Random;
using UnityEngine.EventSystems;
using VIDE_Data;

public class PlayerController : MonoBehaviour
{
    private Rigidbody rb;
    private Animator animator;
    public GameObject phone;
    private LineRenderer connection;
    private Image Blood_L1;
    private Image Blood_L2;
    public int life = 200;
    public float moveSpeed = 0;
    private float rotateSpeed = 2.0f;
    private bool onGround = true;
    /*********************************************************************************************************************/
    public GameObject throw1;
    public GameObject plugerOnHand, spongeOnHand, toiletOnHand, toiletPaperOnHand, toothPasteOnHand, acidOnHand;
    public GameObject spongePrefab, toiletPaperPrefab, toothPasteBullet;
    public int whichWeapon = -1;
    public int toiletPaperNum = -2, acidNum = -2, spongeNum = -2, toiletNum = -2, toothPasteNum = -2;
    public bool haveAcid = false, haveToiletPaper = false, haveSponge = false, haveToilet = false, haveToothPaste = false;
    private Vector3 throwDir;
    public int canUseToiletPaperTimes = 0, canUseSpongeTimes = 0, CanUseToothPasteBullet = 0, CanUseAcidTimes = 0;
    public bool holdingToilet = false;
    private bool throwing = false;
    private bool usingPluger = false;
    private bool usingToothPaste = false;
    private bool usingAcid = false;
    /***************************************************************************************/
    private bool openningConnection = false;
    public int batteryCount = 0;
    private Color startColor = new Color(0, 0, 0, 0); //slighty transparent yellow
    private Color endColor = new Color(0, 0, 0, 0);
    private float startWidth = 1.0f;
    private float endWidth = 1.0f;
    public bool showText = false;
    private bool slipping = false;
    private bool hurt = false;
    public bool becomeDanger = false;
    public bool holdingCandle = false;
    public GameObject candleOnHand;
    public bool getNerdBook = false;
    public bool getTranslator = false;
    public bool getOldPhone = false;
    public bool isPlunge = false;
    private bool isMushroom = false;
    private int mushroomTime = 800;
    /*******************************/
    public bool spiderBlock = false;
    /*******************************/
    public AudioSource audioSource;
    public AudioClip throwSE;
    public AudioClip useAcidSE;
    public AudioClip slippingSE;
    public AudioClip useToothPasteSE;
    public AudioClip useToiletSE;
    public AudioClip PlayerHurtSE;
    public AudioClip PlayerDieSE;
    public AudioClip connectionSE;
    public AudioClip gainLifeSE;
    public AudioClip pickPhone;
    static public bool winGame = false;
    /***************************/
    public AudioSource audioSource_heartBeat;
    private bool flag_heartBeat = false;
    /****************************************/
    public bool Talking = false;
    /************************************************/
    public GameObject hurtPE, mushroomPE, firstAidPE;
    private GameObject mushroomPE_tmp;

    public bool cameraHitRight = false;
    public bool cameraHitLeft = false;

    /**************/
    public GameObject skeleton;
    public GameObject skeleton_first;
    /**************/

    private Vector3 initPos = new Vector3(6, 2, -32);



    void Awake()
    {
        Blood_L1 = GameObject.Find("Canvas/mainPanel/blood/L1").GetComponent<Image>();
        Blood_L2 = GameObject.Find("Canvas/mainPanel/blood/L2").GetComponent<Image>();
    }

    void Start()
    {
        gameObject.transform.position = initPos;
        life = 200;
        rb = GetComponent<Rigidbody>();
        winGame = false;
        animator = gameObject.GetComponent<Animator>();
        //connection
        connection = gameObject.GetComponent<LineRenderer>();
        connection.textureMode = LineTextureMode.Tile;
        connection.SetColors(startColor, endColor);
        connection.SetWidth(startWidth, endWidth);
        connection.positionCount = 0;
        hurtPE.SetActive(false);
        toiletPaperNum = -2;
        acidNum = -2;
        spongeNum = -2;
        toiletNum = -2;
        toothPasteNum = -2;
        haveAcid = false;
        haveToiletPaper = false;
        haveSponge = false;
        haveToilet = false;
        haveToothPaste = false;
        canUseToiletPaperTimes = 0;
        canUseSpongeTimes = 0;
        CanUseToothPasteBullet = 0;
        CanUseAcidTimes = 0;
    }

    void Update()
    {
        // print(life);
        // getOldPhone = true;
        // getTranslator = true;
        //getNerdBook = true;
        if (!hurt)
            hurtPE.SetActive(false);
        /****************************************/
        if (!winGame)
        {
            if (isMushroom)
            {
                mushroomTime--;
                if (mushroomTime <= 0) closeMushroom();
            }
            if (spiderBlock)
            {
                moveSpeed = 0;
                animator.SetFloat("Speed", moveSpeed);
                return;
            }

            if (life <= 0)
            {
                print("!");
                audioSource.PlayOneShot(PlayerDieSE);
                Invoke("toLoseScene", 1.0f);
            }

            if (life <= 100)
            {
                becomeDanger = true;
                if (!flag_heartBeat)
                {
                    flag_heartBeat = true;
                    audioSource_heartBeat.Play();
                }
            }
            else
            {
                flag_heartBeat = false;
                audioSource_heartBeat.Stop();
            }

            if (life > 0 && !slipping && !Talking)
            {
                //walk
                if (Input.GetKey(KeyCode.W))
                {
                    if (!becomeDanger)
                    {
                        moveSpeed = 2.7f;
                    }
                    else
                    {
                        moveSpeed = 3.2f;
                    }
                    transform.Translate(Vector3.forward * Time.deltaTime * moveSpeed);
                }
                else if (Input.GetKey(KeyCode.S) && !cameraHitLeft && !cameraHitRight)
                {
                    if (!becomeDanger)
                    {
                        moveSpeed = 2.7f;
                    }
                    else
                    {
                        moveSpeed = 3.2f;
                    }
                    transform.Translate(Vector3.back * Time.deltaTime * moveSpeed);
                }
                else
                {
                    moveSpeed = 0;
                }
                //run
                if ((Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.LeftShift)) || (Input.GetKey(KeyCode.W) && Input.GetKey(KeyCode.RightShift)))
                {
                    moveSpeed = 4.0f;
                    transform.Translate(Vector3.forward * Time.deltaTime * moveSpeed);
                }
                //rotate
                if (Input.GetKey(KeyCode.A) && !cameraHitLeft)
                {
                    transform.Rotate(new Vector3(0, -rotateSpeed, 0));
                }
                else if (Input.GetKey(KeyCode.D) && !cameraHitRight)
                {
                    transform.Rotate(new Vector3(0, rotateSpeed, 0));
                }
                //jumping
                if (onGround && Input.GetKeyDown(KeyCode.Space))
                {
                    onGround = false;
                    rb.AddForce(new Vector3(0, 5.0f, 0), ForceMode.Impulse);
                }

                if (isPlunge)
                {
                    toiletPaperOnHand.SetActive(false);
                    spongeOnHand.SetActive(false);
                    toiletOnHand.SetActive(false);
                    toothPasteOnHand.SetActive(false);
                    acidOnHand.SetActive(false);
                    plugerOnHand.SetActive(true);
                    if (Input.GetMouseButtonDown(0) && !usingPluger)
                    {
                        usingPluger = true;
                        Invoke("usePluger", 0.5f);
                    }
                }
                else if (whichWeapon == toiletPaperNum)
                {
                    plugerOnHand.SetActive(false);
                    spongeOnHand.SetActive(false);
                    toiletOnHand.SetActive(false);
                    toothPasteOnHand.SetActive(false);
                    acidOnHand.SetActive(false);
                    if (canUseToiletPaperTimes > 0)
                        toiletPaperOnHand.SetActive(true);
                    else
                        toiletPaperOnHand.SetActive(false);
                    holdingToilet = false; // for animation
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit))
                    {
                        if (Input.GetMouseButtonDown(0) && !throwing && haveToiletPaper && canUseToiletPaperTimes > 0 && GameController.canAttack)
                        {
                            throwing = true;
                            throwDir = new Vector3(hit.point.x - transform.position.x, hit.point.y - transform.position.y, hit.point.z - transform.position.z);
                            Invoke("throwToiletPaper", 0.5f);
                        }
                    }
                }
                else if (whichWeapon == acidNum)
                {
                    toiletPaperOnHand.SetActive(false);
                    toiletOnHand.SetActive(false);
                    toothPasteOnHand.SetActive(false);
                    if (CanUseAcidTimes > 0)
                        acidOnHand.SetActive(true);
                    else
                        acidOnHand.SetActive(false);
                    plugerOnHand.SetActive(false);
                    spongeOnHand.SetActive(false);
                    holdingToilet = false; // for animation
                    if (Input.GetMouseButtonDown(0) && haveAcid && GameController.canAttack && !usingAcid)
                    {
                        audioSource.PlayOneShot(useAcidSE);
                        usingAcid = true;
                        acidOnHand.GetComponent<acidController>().open();
                        Invoke("useAcid", 0.1f);
                    }
                }
                else if (whichWeapon == spongeNum)
                {
                    if (canUseSpongeTimes > 0)
                        spongeOnHand.SetActive(true);
                    else
                        spongeOnHand.SetActive(false);

                    toiletPaperOnHand.SetActive(false);
                    toiletOnHand.SetActive(false);
                    toothPasteOnHand.SetActive(false);
                    acidOnHand.SetActive(false);
                    plugerOnHand.SetActive(false);
                    holdingToilet = false; // for animation
                    Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
                    RaycastHit hit;
                    if (Physics.Raycast(ray, out hit))
                    {
                        if (Input.GetMouseButtonDown(0) && !throwing && haveSponge && canUseSpongeTimes > 0 && GameController.canAttack)
                        {
                            throwing = true;
                            throwDir = new Vector3(hit.point.x - transform.position.x, hit.point.y - transform.position.y, hit.point.z - transform.position.z);
                            Invoke("throwSponge", 0.5f);
                        }
                    }
                }
                else if (whichWeapon == toiletNum)
                {
                    toiletPaperOnHand.SetActive(false);
                    spongeOnHand.SetActive(false);
                    toiletOnHand.SetActive(true);
                    toothPasteOnHand.SetActive(false);
                    acidOnHand.SetActive(false);
                    plugerOnHand.SetActive(false);
                    /********************/
                    holdingToilet = true; // for animation
                    /********************/
                    if (Input.GetMouseButtonDown(0) && haveToilet && GameController.canAttack)
                    {
                        audioSource.PlayOneShot(useToiletSE);
                        toiletOnHand.GetComponent<toiletController>().open();
                    }
                    if (Input.GetMouseButtonUp(0) || !GameController.canAttack)
                    {
                        toiletOnHand.GetComponent<toiletController>().close();
                    }
                }
                else if (whichWeapon == toothPasteNum)
                {
                    toiletPaperOnHand.SetActive(false);
                    spongeOnHand.SetActive(false);
                    toiletOnHand.SetActive(false);
                    if (CanUseToothPasteBullet > 0)
                        toothPasteOnHand.SetActive(true);
                    else
                        toothPasteOnHand.SetActive(false);
                    acidOnHand.SetActive(false);
                    plugerOnHand.SetActive(false);
                    holdingToilet = false; // for animation
                    if (Input.GetMouseButtonDown(0) && haveToothPaste && CanUseToothPasteBullet > 0 && GameController.canAttack)
                    {
                        audioSource.PlayOneShot(useToothPasteSE);
                        usingToothPaste = true;
                        Instantiate(toothPasteBullet, transform.position + new Vector3(transform.forward.x * 0.5f, 0.5f, transform.forward.z * 0.5f), transform.rotation);
                        Invoke("useToothPaste", 0.1f);
                    }
                }
                else
                {
                    toiletPaperOnHand.SetActive(false);
                    spongeOnHand.SetActive(false);
                    toiletOnHand.SetActive(false);
                    toothPasteOnHand.SetActive(false);
                    plugerOnHand.SetActive(false);
                    acidOnHand.SetActive(false);
                }
            }
            else
            {
                // print("die");
                animator.SetFloat("Speed", 0);
                animator.SetBool("Jump", false);
                return;
            }

            //holding candle walk
            if (holdingCandle)
            {
                candleOnHand.SetActive(true);
            }

            //animation control
            animator.SetFloat("Speed", moveSpeed);
            animator.SetBool("Jump", !onGround);
            animator.SetBool("Throw", throwing);
            animator.SetBool("UsingPluger", usingPluger);
            animator.SetBool("Slip", slipping);
            animator.SetBool("Hurt", hurt);
            animator.SetBool("FastWalk", becomeDanger);
            animator.SetBool("CandleWalk", holdingCandle);
            animator.SetBool("UsingToilet", holdingToilet);
            animator.SetBool("UsingToothPaste", usingToothPaste);
            animator.SetBool("UsingAcid", usingAcid);
        }
        else
        {
            moveSpeed = 0;
            animator.SetFloat("Speed", moveSpeed);
        }

    }

    void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "ground" || other.gameObject.tag == "desk")
        {
            onGround = true;
        }

        // else if(other.gameObject.tag == "enemy"){
        //     getHit(1);
        // }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "phone")
        {
            Debug.Log("???");
            winGame = true;
            audioSource.Pause();
            audioSource.clip = pickPhone;
            audioSource.Play();
            audioSource.loop = false;
        }

        if (other.tag == "skeleton_part")
        {
            if (skeleton_first.GetComponent<skeleton>().isEnemy) getHit(1);
        }
    }
    void OnCollisionStay(Collision other)
    {
    }

    void CloseConnection()
    {
        connection.positionCount = 0;
    }

    public void OpenConnection()
    {
        print("connection");
        audioSource.PlayOneShot(connectionSE);
        Invoke("CloseConnection", 1f);
        connection = GetComponent<LineRenderer>();
        connection.positionCount = 2;
        if (phone.transform.position.y < 0)
            connection.SetPosition(0, new Vector3(transform.position.x, transform.position.y + 0.6f, transform.position.z));
        else
            connection.SetPosition(0, new Vector3(transform.position.x, transform.position.y, transform.position.z));
        connection.SetPosition(1, phone.transform.position);
    }

    public void addLife()
    {
        GameObject pe = Instantiate(firstAidPE, transform.position + new Vector3(0, 1, 0), Quaternion.identity);
        audioSource.PlayOneShot(gainLifeSE);
        Destroy(pe, 3);
        life += 10;
        life = Mathf.Min(200, life);
        if (life >= 100 && life <= 200)
        {
            Blood_L2.fillAmount = 1.0f;
            Blood_L1.fillAmount += 10 / 100f;
        }
        else
        {
            Blood_L2.fillAmount += 10 / 100f;
        }
        print("add life" + life);
    }

    public void plunge()
    {
        isPlunge = true;
        print("plunge");
    }

    void usePluger()
    {
        usingPluger = false;
        isPlunge = false;
        plugerOnHand.SetActive(false);
    }
    public void useMushroom()
    {
        isMushroom = true;
        mushroomTime = 200;
        mushroomPE_tmp = Instantiate(mushroomPE, transform.position + new Vector3(0, 1, 0), Quaternion.identity);
        mushroomPE_tmp.transform.SetParent(transform);
    }
    void closeMushroom()
    {
        Destroy(mushroomPE_tmp);
        isMushroom = false;
    }
    public void callSkeleton()
    {
        skeleton.SetActive(true);
        skeleton.transform.position = new Vector3(transform.position.x + 1, transform.position.y + 0.1f, transform.position.z);
    }

    void throwToiletPaper()
    {
        canUseToiletPaperTimes--;
        if (canUseToiletPaperTimes == 0)
        {
            haveToiletPaper = false;
            whichWeapon = -1;
        }
        throwing = false;
        toiletPaperOnHand.SetActive(false);
        GameObject toiletPaperThrow = Instantiate(toiletPaperPrefab, throw1.transform.position, Quaternion.identity);
        toiletPaperThrow.GetComponent<Rigidbody>().AddForce(throwDir * 5500.0f * Time.deltaTime);
        /********************************/
        audioSource.PlayOneShot(throwSE);
    }

    void useAcid()
    {
        CanUseAcidTimes--;
        if (CanUseAcidTimes == 0)
        {
            haveAcid = false;
            whichWeapon = -1;
        }
        usingAcid = false;
    }

    void throwSponge()
    {
        canUseSpongeTimes--;
        if (canUseSpongeTimes == 0)
        {
            haveSponge = false;
            whichWeapon = -1;
        }
        throwing = false;
        spongeOnHand.SetActive(false);
        GameObject prefab = Instantiate(spongePrefab, throw1.transform.position, Quaternion.identity);
        prefab.GetComponent<Rigidbody>().AddForce(throwDir * 5500.0f * Time.deltaTime);
        /********************************/
        audioSource.PlayOneShot(throwSE);
    }

    void useToothPaste()
    {
        CanUseToothPasteBullet--;
        if (CanUseToothPasteBullet == 0)
        {
            haveToothPaste = false;
            whichWeapon = -1;
        }
        usingToothPaste = false;
    }

    /***************************************************************************/
    public void getHit(int num)
    {
        if (!isMushroom)
        {
            // GameObject pe = Instantiate(hurtPE, transform.position + new Vector3(0, 1, 0), Quaternion.identity);
            // Destroy(pe, 3);
            hurtPE.SetActive(true);
            hurt = true;
            life -= num;
            life = Mathf.Max(0, life);
            // print(life);
            if (life >= 100)
            {
                Blood_L1.fillAmount -= num / 100f;
            }
            else
            {
                // if(life == 0)
                // halfblood = true;
                Blood_L1.fillAmount = 0;
                Blood_L2.fillAmount -= num / 100f;
            }
            print("player get hit: " + num);
            Invoke("cancelHurt", 0.1f);
            audioSource.PlayOneShot(PlayerHurtSE);
        }

    }
    public void getHitNoAnimation(int num)
    {
        if (!isMushroom)
        {
            life -= num;
            life = Mathf.Max(0, life);
            if (life >= 100)
            {
                Blood_L1.fillAmount -= num / 100f;
            }
            else
            {
                Blood_L2.fillAmount -= num / 100f;
            }
            print("player get hit no Animation: " + num);
        }
    }

    void cancelHurt()
    {
        hurt = false;
    }

    public void slide()
    {
        audioSource.PlayOneShot(slippingSE);
        slipping = true;
        //moveSpeed = 0;
        int force = 800;
        gameObject.GetComponent<Rigidbody>().AddForce(force * transform.forward);
        Invoke("cancelSlipping", 0.5f);
    }

    void cancelSlipping()
    {
        slipping = false;
    }

    public void spiderAttack(GameObject web)
    {
        if (!isMushroom)
        {
            if (spiderBlock) return;
            web.transform.SetParent(transform);
            web.transform.localPosition = new Vector3(0, 1, -0.1f);
            web.transform.localRotation = Quaternion.Euler(-90, 0, 0);
            getHit(5);
            Quaternion r = transform.rotation;
            transform.rotation = Quaternion.Euler(90, r.y, r.z);
            spiderBlock = true;
        }
    }
    public void spiderAttackDone()
    {
        if (spiderBlock)
        {
            spiderBlock = false;
            Quaternion r = transform.rotation;
            transform.rotation = Quaternion.Euler(0, r.y, r.z);
        }

    }

    void toLoseScene()
    {
        SceneManager.LoadScene("LoseScene");
    }

}

