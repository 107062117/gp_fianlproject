﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using VIDE_Data;

public class DialogueController : MonoBehaviour
{
    public GameObject dialogueContainer;
    public GameObject EnemyContainer;
    public GameObject PlayerContainer;
    public Text EnemyText;
    public Text EnemyName;
    public Image EnemyImage;
    public GameObject[] PlayerChoice = new GameObject[3];
    // public Image PlayerImage;
    public Text playerName;
    public GameObject PlayerText;  // itempopup
    // public GameObject PressEnter;
    public GameObject info;
    public PlayerController player;
    public skeleton skeleton;
    public SpiderQueen queen;
    public Santa santa;
    public doorcontroller door;
    public GameObject seeMsgText;

    private bool dialoguePaused = false;
    private bool animatingText = false;
    private List<Text> currentChoices = new List<Text>();
    private string[] yesno = new string[] { "當然好啊！", "不是很想..." };
    private bool askTranslator = false;
    private bool askOldPhone = false;
    private bool NeedToAsk0 = false;
    private bool NeedToAsk1 = false;
    private bool isAdd = false;
    private bool changeCondition = false;

    public BackPackController backPack;
    public AudioSource audioSource;
    public AudioClip talkingSE;

    private GameObject collider;

    IEnumerator EnemyTextAnimator;

    void Awake()
    {
        VD.LoadState("Dialogue", true);
        foreach (GameObject c in PlayerChoice)
        {
            c.SetActive(false);
        }

        PlayerText.SetActive(false);
        askTranslator = false;
        NeedToAsk0 = false;
        NeedToAsk1 = false;
        isAdd = false;
    }

    void Update()
    {
        var data = VD.nodeData;

        if (VD.isActive)
        {
            if (!data.pausedAction && data.isPlayer)
            {
                if (((NeedToAsk0 && !askTranslator) || (NeedToAsk1 && !askOldPhone)) && !isAdd)
                {
                    data.commentIndex += 2;
                    isAdd = true;
                }
                if (Input.GetKeyDown(KeyCode.S))
                {
                    if (data.commentIndex < currentChoices.Count - 1)
                        data.commentIndex++;
                }
                else if (Input.GetKeyDown(KeyCode.W))
                {
                    if (data.commentIndex > 0)
                        data.commentIndex--;
                }

                //Color the Player options. Blue for the selected one
                for (int i = 0; i < currentChoices.Count; i++)
                {
                    currentChoices[i].color = new Color(0.2f, 0.2f, 0.2f, 1f);
                    if (i == data.commentIndex) currentChoices[i].color = new Color(0.36f, 0.21f, 0.18f, 1f);
                }
            }
        }
    }
    public void Interact(VIDE_Assign dialogue)
    {
        //Sometimes, we might want to check the ExtraVariables and VAs before moving forward
        //We might want to modify the dialogue or perhaps go to another node, or dont start the dialogue at all
        //In such cases, the function will return true
        if (PreConditions(dialogue)) return;

        if (!VD.isActive)
        {
            print("begin");
            Begin(dialogue);
        }
        else
        {
            // print("callnext");
            CallNext();
            info.SetActive(false);
        }
    }

    void Begin(VIDE_Assign dialogue)
    {
        info.SetActive(true);
        EnemyText.text = "";
        EnemyName.text = "";
        // playerLabel.text = "";

        VD.OnActionNode += ActionHandler;
        VD.OnNodeChange += UpdateUI;
        VD.OnEnd += EndDialogue;
        audioSource.PlayOneShot(talkingSE, 0.5f);

        VD.BeginDialogue(dialogue);
        player.Talking = true;
        isAdd = false;
    }

    public void CallNext()
    {
        foreach (GameObject c in PlayerChoice)
        {
            c.SetActive(false);
        }
        if (animatingText) { CutTextAnim(); return; }

        if (!dialoguePaused) VD.Next();
        else
        {
            PlayerText.SetActive(false);
            if (PlayerText.activeSelf)
            {
                dialoguePaused = false;
                // PlayerText.SetActive(false);
            }
        }


    }

    void UpdateUI(VD.NodeData data)
    {
        currentChoices = new List<Text>();
        EnemyText.text = "";
        EnemyContainer.SetActive(false);
        PlayerContainer.SetActive(false);
        EnemyImage.sprite = null;
        // print(VD.nodeData.nodeID);

        // Player
        if (data.isPlayer)
        {
            // print("isplayer");
            // Set player name
            if (data.tag.Length > 0)
                playerName.text = data.tag;

            askTranslator = false;
            askOldPhone = false;
            // check translator
            if (data.extraVars.ContainsKey("translator"))
            {
                NeedToAsk0 = true;
                if (player.getTranslator) askTranslator = true;
                else askTranslator = false;
            }
            else NeedToAsk0 = false;

            // check translator
            if (data.extraVars.ContainsKey("OldPhone"))
            {
                NeedToAsk1 = true;
                if (player.getOldPhone) askOldPhone = true;
                else askOldPhone = false;
            }
            else NeedToAsk1 = false;

            SetOptions(data.comments, askTranslator, NeedToAsk0, NeedToAsk1);
            PlayerContainer.SetActive(true);

        }
        // Enemy
        else
        {
            // print("isenemy");
            // Set Enemy name
            if (data.tag.Length > 0)
                EnemyName.text = data.tag;
            else
                EnemyName.text = VD.assigned.alias;

            // Set Enemy img
            if (data.sprite != null)
                EnemyImage.sprite = data.sprite;
            else if (VD.assigned.defaultNPCSprite != null)
                EnemyImage.sprite = VD.assigned.defaultNPCSprite;
            EnemyTextAnimator = DrawText(data.comments[data.commentIndex], 0.02f);
            StartCoroutine(EnemyTextAnimator);

            EnemyContainer.SetActive(true);
        }
    }

    public void SetOptions(string[] choices, bool askTranslator, bool NeedToAsk0, bool NeedToAsk1)
    {
        if (NeedToAsk0)
        {
            playerName.text = "";
            if (askTranslator)
            {
                // print("enter ask use or not");
                PlayerText.SetActive(true);
                PlayerText.GetComponent<Text>().text = "您有道具：<color=#619f6cff>翻譯機</color>，請問要使用嗎？";
            }
            else
            {
                PlayerText.SetActive(true);
                PlayerText.GetComponent<Text>().text = "需要翻譯機呢。撿到再去找志祥吧。";
                // print("sorry");
                return;
            }
        }
        else if (NeedToAsk1)
        {
            playerName.text = "";
            if (askOldPhone)
            {
                // print("enter ask use or not");
                PlayerText.SetActive(true);
                PlayerText.GetComponent<Text>().text = "您有道具：<color=#619f6cff>舊手機</color>，請問要給骷髏嗎？";
            }
            else
            {
                PlayerText.SetActive(true);
                PlayerText.GetComponent<Text>().text = "您的背包中並沒有骷髏的手機。";
                // print("sorry");
                return;
            }
        }
        else
        {
            PlayerText.SetActive(false);
        }

        float x = PlayerChoice[1].transform.position.x;
        float y = PlayerChoice[1].transform.position.y;
        float z = PlayerChoice[1].transform.position.z;

        for (int i = 0; i < choices.Length; i++)
        {
            if (choices[i] == "")
            {  // 0
                continue;
            }
            else
            {
                if (askTranslator || askOldPhone){
                    PlayerChoice[i].GetComponent<Text>().alignment = TextAnchor.MiddleCenter;
                }
                else{
                    PlayerChoice[i].GetComponent<Text>().alignment = TextAnchor.MiddleLeft;
                }
                PlayerChoice[i].GetComponent<Text>().text = choices[i];
                PlayerChoice[i].SetActive(true);
                // PlayerChoice[i].transform.parent.gameObject.SetActive(true);
                currentChoices.Add(PlayerChoice[i].GetComponent<Text>());
            }
        }
    }

    bool PreConditions(VIDE_Assign dialogue)
    {
        var data = VD.nodeData;

        if (VD.isActive)
        {
            // Enemy gives items
            if (!data.isPlayer && !data.dirty){
                if(!data.dirty){
                    if(data.extraVars.ContainsKey("nerdbook")){
                        print(player.getNerdBook);
                        if (player.getNerdBook)
                        {
                            VD.SetNode(13);
                            changeCondition = true;
                            return true;
                        }
                        else
                        {
                            VD.SetNode(14);
                        }
                    }else if(data.extraVars.ContainsKey("enemy")){
                        if((int)data.extraVars["enemy"] == data.commentIndex){
                            skeleton.isEnemy = true;
                            skeleton.isFriend = false;
                            print("skeleton enemy");
                            // return true;
                        }
                    }else if(data.extraVars.ContainsKey("friend")){
                        if((int)data.extraVars["friend"] == data.commentIndex){
                            skeleton.isFriend = true;
                            skeleton.isEnemy = false;
                            player.Talking = false;
                            print("skeleton friend");
                            // return true;
                        }
                    }else if(data.extraVars.ContainsKey("startAttack")){
                        if((int)data.extraVars["startAttack"] == data.commentIndex){
                            queen.startAttack = true;
                            print("queen");
                            // return true;
                        }
                    }else if(data.extraVars.ContainsKey("isOpen")){
                        if((int)data.extraVars["isOpen"] == data.commentIndex){
                            door.isopen = true;
                            print("open");
                        }
                    }
                    else if(data.extraVars.ContainsKey("useIt_t")){
                        backPack.deletePropString("translator");
                    }
                    else if(data.extraVars.ContainsKey("useIt_p")){
                        backPack.deletePropString("Old Phone");
                    }
                }
                
                if(data.extraVars.ContainsKey("second")){
                    if((int)data.extraVars["second"] == data.commentIndex){
                        santa.second_talk = true;
                    }
                }
                
            }
        }
        return false;
    }

    void GiveItem(int itemIndex)
    {
        // player.demo_ItemInventory.Add(player.demo_Items[itemIndex]); // 待
        PlayerText.SetActive(true);
        // string text = "You've got a <color=yellow>" + player.demo_Items[itemIndex] + "</color>!";  // 待
        PlayerText.GetComponent<Text>().text = "text";
        dialoguePaused = true;
    }

    // Text animation
    IEnumerator DrawText(string text, float time)
    {
        animatingText = true;
        string[] words = text.Split(' ');

        for (int i = 0; i < words.Length; i++)
        {
            string word = words[i];
            if (i != words.Length - 1) word += " ";

            string previousText = EnemyText.text;

            float lastHeight = EnemyText.preferredHeight;
            EnemyText.text += word;
            if (EnemyText.preferredHeight > lastHeight)
            {
                previousText += System.Environment.NewLine;
            }

            for (int j = 0; j < word.Length; j++)
            {
                EnemyText.text = previousText + word.Substring(0, j + 1);
                yield return new WaitForSeconds(time);
            }
        }
        EnemyText.text = text;
        animatingText = false;
    }

    //Let's not go forward if text is currently being animated, but let's speed it up.
    void CutTextAnim()
    {
        StopCoroutine(EnemyTextAnimator);
        EnemyText.text = VD.nodeData.comments[VD.nodeData.commentIndex];
        animatingText = false;
    }


    //Check task progression
    void CheckTasks()
    {
        QuestChartDemo.CheckTaskCompletion(VD.nodeData);
    }

    void OnLoadedAction()
    {
        Debug.Log("Finished loading all dialogues");
        VD.OnLoaded -= OnLoadedAction;
    }

    //Another way to handle Action Nodes is to listen to the OnActionNode event, which sends the ID of the action node
    void ActionHandler(int actionNodeID)
    {
        //Debug.Log("ACTION TRIGGERED: " + actionNodeID.ToString());
        if (actionNodeID == 7)
        {

        }
    }

    void EndDialogue(VD.NodeData data)
    {
        print("end");
        // CheckTasks();
        VD.OnActionNode -= ActionHandler;
        VD.OnNodeChange -= UpdateUI;
        VD.OnEnd -= EndDialogue;
        dialogueContainer.SetActive(false);
        collider.SetActive(false);
        player.Talking = false;
        // seeMsgText.SetActive(false);
        
        VD.EndDialogue();

        VD.SaveState("Dialogue", true);
        // QuestChartDemo.SaveProgress(); //saves OUR custom game data
    }

    void OnDisable()
    {
        // CheckTasks();
        print("disable");
        VD.OnActionNode -= ActionHandler;
        VD.OnNodeChange -= UpdateUI;
        VD.OnEnd -= EndDialogue;
        if (dialogueContainer != null)
        {
            dialogueContainer.SetActive(false);
            player.Talking = false;
        }
        VD.EndDialogue();
    }

    public void GetCollider(GameObject name){
        collider = name;
    }
}
