﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpiderWeb : MonoBehaviour
{
    private float speed = 15.0f;
    private string state;
    private int timer = 20;
    private GameObject player;

    void Start()
    {
        state = "init";
        player = GameObject.Find("Player");
    }
    private void OnTriggerEnter(Collider other)
    {
        if (state != "active")
        {
            if (other.gameObject.tag == "Player")
            {
                state = "active";
                other.GetComponent<PlayerController>().spiderAttack(gameObject);
                Invoke("done", 1);
            }
            else if (other.gameObject.name != "spider")
            {
                Destroy(gameObject);
            }
        }

    }
    void done()
    {
        player.GetComponent<PlayerController>().spiderAttackDone();
        Destroy(gameObject, 0.1f);
    }
    void Update()
    {

        if (state == "init")
        {
            timer--;
            if (timer <= 0)
            {
                // player.GetComponent<PlayerController>().spiderAttackDone();
                // print("init");
                Destroy(gameObject);
            }
            gameObject.transform.Translate(new Vector3(0, 0, speed * Time.deltaTime));
        }
    }
}
