﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class skeleton : MonoBehaviour
{
    public Sprite img;
    private Animator anim;
    private AnimatorClipInfo[] m_CurrentClipInfo;
    private GameObject blood, type;
    private Image Blood;
    private bool BloodAppear;
    private int life = 40;
    public bool isFriend = true;
    public bool isEnemy = false;
    private float speed = 1f;
    private bool onCollision;
    public GameObject santa;
    public GameObject HitEffect;
    public bool isRunning = false;
    private GameObject player;

    void Awake()
    {
        blood = GameObject.Find("Canvas/mainPanel/blood_e/blood");
        Blood = blood.GetComponent<Image>();
        type = GameObject.Find("Canvas/mainPanel/blood_e/type");
        type.GetComponent<Image>().sprite = img;
        //santa = GameObject.Find("SantaClaus");
        isFriend = true;
        player = GameObject.Find("Player");
    }
    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
        anim.speed = 0.75F;
        BloodAppear = false;   // need to change when environment done
        Blood.fillAmount = 1.0f;
    }

    private void OnCollisionStay(Collision other)
    {
        if (isFriend)
        {
            if (other.gameObject.tag != "ground")
            {
                onCollision = true;
            }
        }
    }

    /******************************/
    void Update()
    {
        if (isFriend)
        {
            if (gameObject.tag == "friend")
            {
                if (!isRunning)
                {
                    isRunning = true;
                    anim.SetTrigger("run");
                }
                run();
            }
        }


        if (isEnemy)
        {
            if (life > 0 && Vector3.Distance(player.transform.position, gameObject.transform.position) < 8)
            {
                type.GetComponent<Image>().sprite = img;
                BloodAppear = true;
            }
            else BloodAppear = false;
            gameObject.tag = "enemy";
        }

        blood.transform.parent.gameObject.SetActive(BloodAppear);
        // print(isFriend);
    }

    void OnCollisionEnter(Collision other)
    {
        if (isFriend)
        {
            if (other.gameObject.name == "SantaClaus")
            {
                gameObject.SetActive(false);
            }
            if (other.gameObject.tag != "ground")
            {
                onCollision = true;
            }
        }
        // Be Hit
        if (other.gameObject.tag == "weapon" && isEnemy)
        {
            getHit(1);
        }
    }

    private void OnCollisionExit(Collision other)
    {
        if (isFriend) onCollision = false;
    }

    public void getHit(int num)
    {
        life -= num;
        life = Mathf.Max(0, life);
        GameObject effect = Instantiate(HitEffect, transform.position + new Vector3(0, 1, 0), Quaternion.identity);
        effect.transform.SetParent(gameObject.transform);
        if (life > 0)
        {
            Blood.fillAmount -= num / 40f;
            anim.SetTrigger("BeHit");
        }
        else
        {
            anim.SetTrigger("KnockDown");
            BloodAppear = false;
            Destroy(gameObject, 1.5f);
        }
    }
    private void run()
    {
        float angle = Vector3.Angle(transform.forward, santa.transform.position - transform.position);
        angle = Vector3.Dot(Vector3.right, transform.InverseTransformPoint(santa.transform.position)) < 0 ? -angle : angle;
        float dis = Vector3.Distance(gameObject.transform.position, santa.transform.position);
        if (dis < 5) speed = 3.0f;
        else speed = 1f;
        gameObject.transform.Translate(new Vector3(0, 0, speed * Time.deltaTime));
        if (angle > 0.0f && angle < 140.0f)
        {
            gameObject.transform.Rotate(new Vector3(0, -5.0f, 0));
        }
        else if (angle <= 0.0f && angle > -140.0f)
            gameObject.transform.Rotate(new Vector3(0, 5.0f, 0));

        if (onCollision) gameObject.transform.Rotate(new Vector3(0, Random.Range(-180, 180), 0));
    }
    /******************************/

    public void startAttack()
    {
        float x = player.gameObject.transform.position.x;
        float y = gameObject.transform.position.y;
        float z = player.gameObject.transform.position.z;
        gameObject.transform.LookAt(new Vector3(x, y, z));
        m_CurrentClipInfo = anim.GetCurrentAnimatorClipInfo(0);
        string name = m_CurrentClipInfo[0].clip.name;
        if (name == "SimpleSkeleton_idle_upright" || name == "SimpleSkeleton_punch_idle")
        {
            float num = Mathf.Round(Random.Range(0f, 1f));
            if (num == 0f) anim.SetTrigger("punch_L");
            else anim.SetTrigger("punch_R");
        }
    }
}

