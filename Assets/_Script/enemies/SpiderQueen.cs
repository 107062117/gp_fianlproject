﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpiderQueen : MonoBehaviour
{
    public GameObject player;
    private int timer;
    private int initTimer = 60;
    private string state;
    private float distance = 100.0f;
    private Vector3 direction;
    private Animator animator;
    private float speed = 1.0f;
    private GameObject SpiderQueenBody;
    private bool isPlayerHit;
    private int attackTime;
    private int life = 15;
    public GameObject prefab;
    public GameObject HitEffect;
    public GameObject AttackEffect;
    private GameObject attack_PE;
    private bool attackPECreate = false;
    /**********************************/
    public AudioSource audioSource;
    public AudioClip SpiderQueenHurtSE;
    public AudioClip SpiderQueenDieSE;

    /***************/
    public bool startAttack = false;
    private GameObject blood, type;
    private Image Blood;
    private bool BloodAppear;
    public Sprite img;
    /***************/

    void Awake()
    {
        blood = GameObject.Find("Canvas/mainPanel/blood_e/blood");
        Blood = blood.GetComponent<Image>();
        type = GameObject.Find("Canvas/mainPanel/blood_e/type");
        type.GetComponent<Image>().sprite = img;
    }

    void Start()
    {
        state = "idle";
        timer = initTimer;
        SpiderQueenBody = transform.Find("SpiderQueenBody").gameObject;
        animator = SpiderQueenBody.GetComponent<Animator>();
        isPlayerHit = false;
        attackTime = 1;
        BloodAppear = true;   // need to change when environment done
        Blood.fillAmount = 1.0f;
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            isPlayerHit = true;
        }
    }
    private void OnCollisionExit(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            isPlayerHit = false;
        }
    }
    private void attack()
    {
        player.GetComponent<PlayerController>().getHit(5);
    }
    // Update is called once per frame
    void Update()
    {
        if (Vector3.Distance(player.transform.position, transform.position) < 25 && life > 0 && startAttack)
        {
            type.GetComponent<Image>().sprite = img;
            BloodAppear = true;
        }

        else BloodAppear = false;

        blood.transform.parent.gameObject.SetActive(BloodAppear);
        if (life > 0)
        {
            switch (state)
            {
                case "idle":
                    animator.SetFloat("condition", 0.0f);
                    timer = initTimer;
                    if ((Vector3.Distance(transform.position, player.transform.position) < distance) && startAttack)
                    {
                        direction = player.transform.position - transform.position;
                        gameObject.transform.forward = direction;
                        state = "attack";
                    }
                    break;
                case "waiting":
                    if (!attackPECreate)
                    {
                        attack_PE = Instantiate(AttackEffect, transform.position + new Vector3(0, 0.1f, 0), Quaternion.Euler(90, 0, 0));
                        attack_PE.transform.SetParent(transform);
                        attackPECreate = true;
                    }

                    animator.SetFloat("condition", 0.0f);
                    if (Vector3.Distance(transform.position, player.transform.position) > distance)
                        state = "idle";
                    timer--;
                    direction = player.transform.position - transform.position;
                    transform.forward = direction;
                    if (timer <= 70)
                        state = "notice";
                    break;
                case "notice":
                    timer--;
                    if (timer <= 60)
                        state = "attack";
                    break;
                case "attack":
                    if (attackPECreate)
                    {
                        Destroy(attack_PE);
                        attackPECreate = false;
                    }
                    animator.SetFloat("condition", 0.11f);
                    timer--;
                    float dis = Vector3.Distance(transform.position, player.transform.position);
                    if (dis > 0.5 && !isPlayerHit)
                    {
                        //gameObject.transform.Translate(speed * Time.deltaTime * direction, Space.World);
                        gameObject.GetComponent<Rigidbody>().velocity = speed * direction;
                    }
                    if (isPlayerHit)
                    {
                        attackTime--;
                        if (attackTime <= 0)
                        {
                            attack();
                            attackTime = 10;
                        }
                    }
                    if (timer <= 0 && dis > 1)
                    {
                        attackTime = 1;
                        timer = 100;
                        state = "waiting";
                    }
                    break;
            }
        }

    }
    /***************************/
    public void getHit(int num)
    {
        life -= num;
        life = Mathf.Max(0, life);
        GameObject effect = Instantiate(HitEffect, transform.position + new Vector3(0, 0.3f, 0), Quaternion.identity);
        effect.transform.SetParent(gameObject.transform);
        if (life > 0)
        {
            Blood.fillAmount -= num / 15f;
            audioSource.PlayOneShot(SpiderQueenHurtSE);
            animator.SetFloat("condition", 0.21f);
        }
        else
        {
            BloodAppear = false;
            audioSource.PlayOneShot(SpiderQueenDieSE);
            prefab.SetActive(true);
            prefab.transform.position = gameObject.transform.position;
            animator.SetFloat("condition", 0.31f);
            Destroy(gameObject, 2.5f);
        }
    }
}
