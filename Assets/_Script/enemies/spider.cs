﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spider : MonoBehaviour
{
    public GameObject web_prefab;
    private int timer;
    private int rotate_timer;
    private int init_rotate_time;
    private int initTimer = 50;
    private float speed = 2.0f;
    private bool isCollision;
    private Animator animator;
    private int life = 5;
    public GameObject prefab;
    public GameObject HitEffect;
    public SpiderQueen queen;
    private bool start = false;

    void Start()
    {
        timer = initTimer;
        timer += Random.Range(-10, 10);
        isCollision = false;
        animator = gameObject.GetComponent<Animator>();
        init_rotate_time = Random.Range(100, 200);
        rotate_timer = init_rotate_time;
        animator.enabled = false;
    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag != "ground") isCollision = true;
    }
    private void OnCollisionExit(Collision other)
    {
        isCollision = false;
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag != "ground")
        {
            isCollision = true;
        }
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag != "ground")
        {
            isCollision = false;
        }
    }
    void Update()
    {
        if (queen)
        {
            start = queen.startAttack;
        }
        if (life > 0 && start)
        {
            animator.enabled = true;
            //movement
            animator.SetFloat("condition", 0.0f);
            timer--;
            GetComponent<Rigidbody>().velocity = transform.forward * speed;
            if (timer <= 0)
            {
                Instantiate(web_prefab, transform.position, transform.rotation);
                animator.SetFloat("condition", 0.11f);
                timer = initTimer;
            }
            rotate_timer--;
            if (rotate_timer <= 0)
            {
                transform.Rotate(new Vector3(0, Random.Range(-180, 180), 0));
                rotate_timer = init_rotate_time;
            }

            if (isCollision) transform.Rotate(new Vector3(0, Random.Range(-180, 180), 0));
        }
    }

    /******************************/
    public void getHit(int num)
    {

        life -= num;
        life = Mathf.Max(0, life);
        GameObject effect = Instantiate(HitEffect, transform.position + new Vector3(0, 0.3f, 0), Quaternion.identity);
        effect.transform.SetParent(gameObject.transform);
        if (life > 0)
        {
            animator.SetFloat("condition", 0.21f);
        }
        else
        {
            //死亡掉道具
            prefab.transform.position = transform.position;
            prefab.SetActive(true);
            animator.SetFloat("condition", 0.31f);
            Destroy(gameObject, 1.5f);
        }
    }
}
