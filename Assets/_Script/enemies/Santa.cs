﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.UI;

public class Santa : MonoBehaviour
{
    /******************/
    public GameObject[] prefab = new GameObject[6];
    public GameObject[] presentOnHand = new GameObject[6];
    public GameObject[] keys = new GameObject[3]; // 0: candy 1: gingerBread 2: sock
    public GameObject[] tools = new GameObject[5]; // need to add
    /******************/
    public GameObject pos;
    public Sprite img;
    public bool CanAction, StartThrow;
    public GameObject skeleton;
    public GameObject msgcollider;

    private GameObject present_t;
    private GameObject dst;
    private GameObject blood, Enemytype;
    private Image Blood;
    private Animator anim;
    private AnimatorStateInfo stateinfo;
    private NavMeshAgent agent;
    private Vector3 dir;
    private int cnt = 0;
    private bool isThrow, BloodAppear, isDead;
    private int life = 50;
    public GameObject HitEffect;
    /******************************/
    public AudioSource audioSource;
    public AudioClip SantaHurtSE;
    public AudioClip SantaDieSE;

    /******************/
    private int[] presentTypelist = new int[] { 0, 0, 1, 0, 2, 0, 1, 1, 0, 0, 2, 0, 1, 1, 2, 0, 1, 0, 1, 1, 0 };
    private int PresentIdx = 0;
    private int TypeIdx = 0;
    private int KeyIdx = 0;
    private GameObject tmpOnHand;
    private Vector3 Goal = new Vector3(0, 0, 0);
    // {2, 2, 2, 1, 1, 1, 1} for test
    // {0, 0, 1, 2, 3, 2, 0, 1, 0, 2, 1} for test
    // {0, 0, 1, 0, 2, 0, 1, 1, 0, 0, 2, 3, 0, 1, 1, 2, 0, 1, 0, 1, 1, 0}; for real
    public bool second_talk;
    public GameObject fire_effect;

    /******************/

    void Awake()
    {
        dst = GameObject.Find("Player");
        blood = GameObject.Find("Canvas/mainPanel/blood_e/blood");
        Blood = blood.GetComponent<Image>();
        Enemytype = GameObject.Find("Canvas/mainPanel/blood_e/type");
        Enemytype.GetComponent<Image>().sprite = img;
        StartThrow = true; // change
        second_talk = false;
        fire_effect.SetActive(false);
    }

    void Start()
    {
        anim = gameObject.GetComponent<Animator>();
        agent = gameObject.GetComponent<NavMeshAgent>();
        isThrow = false;
        CanAction = true;
        for (int i = 0; i < presentOnHand.Length; i++) presentOnHand[i].SetActive(false);
        BloodAppear = false;   // need to change when environment done
        isDead = false;
        Blood.fillAmount = 1.0f;
    }

    void Update()
    {
        if (second_talk) msgcollider.SetActive(true);

        if (CanAction) cnt++;
        else cnt = 0;
        if (Vector3.Distance(dst.transform.position, transform.position) < 20 && life > 0)
        {
            BloodAppear = true;
            Enemytype.GetComponent<Image>().sprite = img;
        }

        else
            BloodAppear = false;
        blood.transform.parent.gameObject.SetActive(BloodAppear);
        stateinfo = anim.GetCurrentAnimatorStateInfo(0);
        // need to change when environment done

        // Gonna throw
        if (cnt >= 125 && !stateinfo.IsName("throw") && !stateinfo.IsName("run") && !stateinfo.IsName("Dance") && CanAction && StartThrow)
        {
            anim.SetTrigger("Throw");
            isThrow = true;
            PresentIdx = (int)(Mathf.Ceil(Random.Range(0.0001f, presentOnHand.Length))) - 1;
            presentOnHand[PresentIdx].SetActive(true);
            tmpOnHand = presentOnHand[PresentIdx];
            dir = new Vector3(Random.Range(-3f, 3f), 0f, Random.Range(-3f, 3f));
            gameObject.transform.LookAt(gameObject.transform.position - dir);
            cnt = 0;
        }

        // Start to run after the player
        if (stateinfo.IsName("run"))
        {
            // if the player take skeleton out and use it
            tmpOnHand.SetActive(false);
            print("!!!!!");

            if (skeleton.activeInHierarchy)
            {
                float playerDist = Vector3.Distance(gameObject.transform.position, dst.transform.position);
                float skeletonDist = Vector3.Distance(gameObject.transform.position, skeleton.transform.position);
                Goal = (playerDist < skeletonDist) ? dst.transform.position : skeleton.transform.position;
            }
            // no skeleton
            else
            {
                print("else");
                Goal = dst.transform.position;
                agent.stoppingDistance = 0f;
            }
            agent.SetDestination(Goal);
            gameObject.transform.LookAt(new Vector3(Goal.x, transform.position.y, Goal.z));

        }
        // Invoke throw
        else if (stateinfo.IsName("throw"))
        {
            if (!CanAction)
            {
                // anim.Play("idle");
                presentOnHand[PresentIdx].SetActive(false);
                cnt = 0;
            }

            gameObject.transform.LookAt(gameObject.transform.position - dir);
            if (isThrow)
            {
                Invoke("throwPresent", 0.3f);
                isThrow = false;
            }
        }

        // Too far, stop running
        if (Goal != new Vector3(0, 0, 0))
        {
            if (Vector3.Distance(gameObject.transform.position, Goal) > 6f)
            {
                agent.SetDestination(gameObject.transform.position);
                anim.SetBool("run", false);
                Goal = new Vector3(0, 0, 0);
            }
        }

    }

    void OnCollisionEnter(Collision other)
    {
        // Be hit
        if (other.gameObject.tag == "weapon")
        {
            getHit(1);
        }
        else if (other.gameObject.tag == "Player")
        {
            if (stateinfo.IsName("run"))
            {
                other.gameObject.GetComponent<PlayerController>().getHit(5);
                other.gameObject.GetComponent<Rigidbody>().AddForce(800 * transform.forward);
            }


            // other.gameObject.GetComponent<Rigidbody>()
        }
    }

    // Throw present
    void throwPresent()
    {
        if (CanAction)
        {
            presentOnHand[PresentIdx].SetActive(false);
            present_t = Instantiate(prefab[PresentIdx], pos.gameObject.transform.position, Quaternion.identity);
            if (TypeIdx == presentTypelist.Length)
            {
                present_t.GetComponent<PresentController>().type = (int)Mathf.Ceil(Random.Range(0.0001f, 3)) - 1; // Choose type0 or type1
                addItem(present_t.GetComponent<PresentController>().type);
            }
            else
            {
                present_t.GetComponent<PresentController>().type = presentTypelist[TypeIdx];
                addItem(present_t.GetComponent<PresentController>().type);
                TypeIdx++;
            }
            present_t.GetComponent<PresentController>().notKinematic = true;
        }

    }

    public void getHit(int num)
    {
        life -= num;
        life = Mathf.Max(0, life);
        GameObject effect = Instantiate(HitEffect, transform.position + new Vector3(0, 1, 0), Quaternion.identity);
        effect.transform.SetParent(gameObject.transform);
        if (life > 0)
        {
            audioSource.PlayOneShot(SantaHurtSE);
            anim.SetTrigger("BeHit");
            gameObject.transform.LookAt(new Vector3(dst.transform.position.x, transform.position.y, dst.transform.position.z));
            Blood.fillAmount -= num / 50f;
        }
        else
        {
            audioSource.PlayOneShot(SantaDieSE);
            if (!isDead)
            {
                anim.SetTrigger("KnockDown");
                isDead = true;
            }
            Destroy(gameObject, 1.5f);
            BloodAppear = false;
        }
    }

    private void addItem(int type)
    {
        Vector3 pPos = present_t.transform.position;
        switch (type)
        {
            case 0:
                print("type0: run");
                break;
            case 1:
                // print("type1: tool");
                int ToolIdx = (int)Mathf.Ceil(Random.Range(0.0001f, tools.Length)) - 1;
                present_t.GetComponent<PresentController>().item = tools[ToolIdx];
                print(tools[ToolIdx].name);
                present_t.GetComponent<PresentController>().item.GetComponent<treasureController>().type = 1;

                break;
            case 2:
                // print("type2: key");
                if (KeyIdx == keys.Length)
                {
                    int i = (int)Mathf.Ceil(Random.Range(0.0001f, keys.Length)) - 1;
                    present_t.GetComponent<PresentController>().item = keys[i];
                    present_t.GetComponent<PresentController>().item.GetComponent<treasureController>().type = 2;
                    print(keys[i].name);
                }
                else
                {
                    present_t.GetComponent<PresentController>().item = keys[KeyIdx];
                    present_t.GetComponent<PresentController>().item.GetComponent<treasureController>().type = 2;
                    print(keys[KeyIdx].name);
                    KeyIdx++;
                }

                break;
            default:
                print("sth wrong!");
                break;
        }
    }
    public void close()
    {
        Invoke("close_after", 2f);
    }
    private void close_after()
    {
        if (fire_effect.activeSelf)
        {
            fire_effect.SetActive(false);
        }
    }

}

