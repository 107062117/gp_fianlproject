﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using VIDE_Data;

public class MsgController : MonoBehaviour
{
    private Camera cam;
    private AnimatorStateInfo stateinfo;
    public GameObject seeMsgText, msg;
    private bool BeTouched, startToTalk;
    private GameObject self;
    public DialogueController diaControl;
    public VIDE_Assign inTrigger;
    public pickUpPanel pickUpPanel;
    public BackPackController BackPack;
    public GameObject queen;
    public GameObject santa;
    public GameObject skeleton;
    private bool stopMsg;


    void Awake()
    {
        cam = GameObject.Find("Player/Main Camera").GetComponent<Camera>();
        stopMsg = false;
    }

    void Start()
    {
        BeTouched = false;
        startToTalk = false;
        seeMsgText.SetActive(false);
        msg.SetActive(false);
    }

    void Update()
    {
        if (gameObject.name == "msgCollider_santa" || gameObject.name == "msgCollider_santa1")
            stateinfo = santa.GetComponent<Animator>().GetCurrentAnimatorStateInfo(0);

        if (BeTouched && Input.GetKeyDown(KeyCode.R))
        {

            // seeMsgText.SetActive(false);
            // print("intrigger  " + inTrigger);
            TryInteract();
        }

        if (msg.activeInHierarchy)
        {
            startToTalk = true;
            if (gameObject.name == "msgCollider_santa" || gameObject.name == "msgCollider_santa1")
                santa.GetComponent<Santa>().CanAction = false;
            //  else if 
        }

        if (startToTalk && !msg.activeInHierarchy)
        {
            startToTalk = false;
            if (gameObject.name == "msgCollider_skeleton")
            {
                if (skeleton.GetComponent<skeleton>().isFriend)
                {
                    pickUpPanel.show("SimpleSkeleton", "Prop");
                    BackPack.getProp("SimpleSkeleton");
                    Destroy(skeleton);
                    // Destroy(gameObject, 0.5f);
                }
                if (skeleton.GetComponent<skeleton>().isEnemy)
                {
                    // Destroy(gameObject, 0.5f);
                }

            }
            else if (gameObject.name == "msgCollider_santa")
            {
                // if(santa.GetComponent<Santa>().second_talk)
                // Destroy(gameObject, 0.1f);

            }


            if (gameObject.name == "msgCollider_santa" || gameObject.name == "msgCollider_santa1")
                santa.GetComponent<Santa>().CanAction = true;
            // else if(gameObject.name == "msgCollider_queen")
            // {
            //     Destroy(gameObject);
            // }
        }

        if (gameObject.name == "msgCollider_queen")
        {
            if (queen.GetComponent<SpiderQueen>().startAttack)
            {
                if (!msg.activeInHierarchy)
                {
                    // print("!");
                    BeTouched = false;
                    stopMsg = true;
                    Destroy(gameObject);
                }
            }
        }

        if (gameObject.name == "msgCollider_skeleton")
        {
            if (skeleton.GetComponent<skeleton>().isEnemy)
            {
                if (!msg.activeInHierarchy)
                {
                    // print("!");
                    BeTouched = false;
                    stopMsg = true;
                }
            }
        }
    }


    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            // self = gameObject;
            GameObject Parent;
            switch (gameObject.name)
            {
                case "msgCollider_santa":
                    Parent = santa;
                    break;
                case "msgCollider_santa1":
                    Parent = santa;
                    break;
                case "msgCollider_skeleton":
                    Parent = skeleton;
                    break;
                case "msgCollider_queen":
                    Parent = queen;
                    break;
                default:
                    Parent = queen;
                    break;
            }

            if (Parent.GetComponent<VIDE_Assign>() != null)
            {
                print("!");
                inTrigger = Parent.GetComponent<VIDE_Assign>();
                print(inTrigger);
                if (gameObject.name != "msgCollider_santa1")
                {
                    other.gameObject.GetComponent<PlayerController>().Talking = true;
                    Invoke("TryInteract", 0.2f);
                    other.gameObject.transform.LookAt(new Vector3(Parent.transform.position.x, other.gameObject.transform.position.y, Parent.transform.position.z));
                }
            }

            if (gameObject.name != "msgCollider_santa" && gameObject.name != "msgCollider_santa1")
            {
                if (gameObject.name == "msgCollider_skeleton")
                {
                    if (skeleton.GetComponent<skeleton>().isEnemy)
                    {
                        skeleton.GetComponent<skeleton>().startAttack();
                        return;
                    }
                }
                else if (gameObject.name == "msgCollider_queen")
                {
                    if (stopMsg)
                    {
                        // BeTouched = false;
                        return;
                    }
                }
                BeTouched = true;
            }
            else
            {
                if (!stateinfo.IsName("run"))
                {
                    if (gameObject.name == "msgCollider_santa1") seeMsgText.SetActive(true);
                    BeTouched = true;
                    santa.GetComponent<Santa>().CanAction = false;
                }
            }
        }
    }

    void OnTriggerStay(Collider other)
    {
        if (other.gameObject.name == "Player")
        {
            if (gameObject.name == "msgCollider_skeleton")
            {
                if (skeleton.GetComponent<skeleton>().isEnemy)
                {
                    skeleton.GetComponent<skeleton>().startAttack();
                }
            }
            else if (gameObject.name == "msgCollider_queen")
            {
                if (stopMsg)
                {
                    // BeTouched = false;
                    return;
                }
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            print("exit");
            inTrigger = null;
            seeMsgText.SetActive(false);
            BeTouched = false;
            msg.SetActive(false);
            if (gameObject.name == "msgCollider_santa" || gameObject.name == "msgCollider_santa1")
                santa.GetComponent<Santa>().CanAction = true;
        }
    }

    void msgPos()
    {
        GameObject parent;
        switch (gameObject.name)
        {
            case "msgCollider_santa":
                parent = santa;
                break;
            case "msgCollider_santa1":
                parent = santa;
                break;
            case "msgCollider_skeleton":
                parent = skeleton;
                break;
            case "msgCollider_queen":
                parent = queen;
                break;
            default:
                parent = queen;
                break;
        }
        Vector3 pos = cam.WorldToScreenPoint(parent.transform.position);
        float xp = cam.transform.parent.position.x;
        float yp = parent.transform.position.y;
        float zp = cam.transform.parent.position.z;
        msg.transform.position = new Vector3(pos.x, msg.transform.position.y, pos.z);
        cam.transform.parent.LookAt(new Vector3(parent.transform.position.x, cam.transform.parent.position.y, parent.transform.position.z));
        parent.transform.LookAt(new Vector3(xp, yp, zp));
        cam.transform.parent.gameObject.GetComponent<PlayerController>().Talking = true;
    }

    void TryInteract()
    {
        print(inTrigger);
        if (inTrigger)
        {
            msg.SetActive(true);
            msgPos();
            diaControl.Interact(inTrigger);
            diaControl.GetCollider(gameObject);
            return;
        }
    }
}