﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bat : MonoBehaviour
{
    // private Vector3 dir;
    private float speed = 1.0f;
    private GameObject Parent;
    private int cnt = 0;
    private bool flag = false;
    private int life = 10;
    public GameObject HitEffect;

    void Start()
    {
        // dir.x = Random.Range(0f, 1f);
        // dir.z = Random.Range(0f, 1f);
        // dir.y = 0f;
        Parent = gameObject.transform.parent.gameObject;
    }

    void Update()
    {
        Parent.transform.Translate(Vector3.forward * speed * Time.fixedDeltaTime, Space.Self);
        cnt++;
        if (cnt == 100)
        {
            cnt = 0;
            flag = true;
        }

        if (flag)
        {
            Vector3 scale = Parent.transform.localScale;
            scale.z *= -1;
            Parent.transform.localScale = scale;
            speed = -speed;
            flag = false;
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.tag == "environment")
        {
            Vector3 scale = Parent.transform.localScale;
            scale.z *= -1;
            Parent.transform.localScale = scale;
            speed = -speed;
        }
    }
    /***************************/
    public void getHit(int num)
    {

        life -= num;
        life = Mathf.Max(0, life);
        GameObject effect = Instantiate(HitEffect, transform.position + new Vector3(0, 0.3f, 0), Quaternion.identity);
        effect.transform.SetParent(gameObject.transform);
        if (life > 0)
        {

        }
        else
        {
            Destroy(gameObject, 1);
        }
    }
}
