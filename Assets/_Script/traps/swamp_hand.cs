﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class swamp_hand : MonoBehaviour
{
    // Start is called before the first frame update
    private int timer;
    private int initTimer;
    private float raiseUpTime = 50 / 0.8f;
    private string direction;
    void Start()
    {
        timer = Random.Range(200, 500);
        initTimer = timer;
        direction = "up";
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerController>().getHit(2);
        }
    }

    // Update is called once per frame
    void Update()
    {
        timer--;
        if (timer <= raiseUpTime)
        {
            Vector3 scale = gameObject.transform.localScale;
            if (direction == "up")
            {
                scale.y += 0.8f;
            }
            else
            {
                scale.y -= 0.8f;
            }

            gameObject.transform.localScale = scale;
            if (timer == 0)
            {
                timer = initTimer;
                direction = (direction == "up") ? "down" : "up";
            }

        }
    }
}
