﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class trap_poo : MonoBehaviour
{
    // Start is called before the first frame update
    private ParticleSystem part;
    public List<ParticleCollisionEvent> collisionEvents;
    void Start()
    {
        part = GetComponent<ParticleSystem>();
        collisionEvents = new List<ParticleCollisionEvent>();
    }
    private void OnParticleCollision(GameObject other)
    {
        int numCollisionEvents = part.GetCollisionEvents(other, collisionEvents);
        Rigidbody rb = other.GetComponent<Rigidbody>();
        int i = 0;

        while (i < numCollisionEvents)
        {
            if (rb)
            {

                if (other.tag == "Player")
                {
                    Vector3 vec = other.transform.forward;
                    rb.AddForce(vec * -2000);
                    other.GetComponent<PlayerController>().getHit(1);
                }
            }
            i++;
            break;
        }
    }
    // Update is called once per frame
    void Update()
    {

    }
}
