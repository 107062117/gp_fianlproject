﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using Random = UnityEngine.Random;

public class trap_duck : MonoBehaviour
{
    public GameObject duck_prefab;
    public GameObject explosion_prefab;
    int prefabNum = 10;
    int distance = 10;
    bool isAttacked = false;

    public GameObject Player;
    public GameObject pickUpEffect;

    /******************************/
    public AudioSource audioSource;
    public AudioClip duckSE;

    void Start()
    {
        pickUpEffect.SetActive(false);
    }
    private void OnCollisionEnter(Collision other)
    {
        if (other.gameObject.tag == "Player" && !isAttacked)
        {
            audioSource.PlayOneShot(duckSE);
            isAttacked = true;
            attack();
        }
    }
    private void attack()
    {
        for (int i = 0; i < prefabNum; i++)
        {
            int x = Random.Range(-distance, distance);
            int z = Random.Range(-distance, distance);
            int y = Random.Range(1000, 10000);
            int rotate = Random.Range(-180, 180);
            GameObject duck = Instantiate(duck_prefab, transform.position + new Vector3(0, 0f, 0), Quaternion.Euler(0, rotate, 0));
            duck.GetComponent<Rigidbody>().AddForce(new Vector3(x, y, z));
        }
        Instantiate(explosion_prefab, transform.position, Quaternion.identity);
        Player.GetComponent<PlayerController>().getHit(4);
        Destroy(gameObject);
    }

    void Update()
    {
        double dist = Math.Sqrt(Math.Pow((transform.position.x - Player.transform.position.x), 2) + Math.Pow((transform.position.z - Player.transform.position.z), 2));
        if (dist <= 5)
        {
            pickUpEffect.SetActive(true);
        }
        else
        {
            pickUpEffect.SetActive(false);
        }
    }
}
