﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;
using Random = UnityEngine.Random;

public class trap_soap : MonoBehaviour
{
    public GameObject Player;
    public GameObject pickUpEffect;

    void Start()
    {
        pickUpEffect.SetActive(false);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.GetComponent<PlayerController>().slide();
        }
    }

    // Update is called once per frame
    void Update()
    {
        double dist = Math.Sqrt(Math.Pow((transform.position.x-Player.transform.position.x), 2) + Math.Pow((transform.position.z-Player.transform.position.z), 2));
        if(dist <= 5) {
            pickUpEffect.SetActive(true);
        }
        else {
            pickUpEffect.SetActive(false);
        }
    }
}
